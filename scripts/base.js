
$(document).ready(() => {

    // radio-buttons для главной страницы
    // $('.sizes-radio').on('click', function () {
    //     const ths = $(this);
    //     ths.siblings('.sizes-radio').removeClass('active');
    //     ths.toggleClass('active');
    //     ths.parents(".swiper-item2").siblings().find(".sizes-radio").removeClass("active");
    // });

    // tabs для карточки товара
    $(".js-tab-trigger").on("click", function () {
        const tabName = $(this).data("tab");
        const tab = $('.js-tab-content[data-tab="' + tabName + '"]');

        $(".js-tab-trigger.active-tab").removeClass("active-tab");
        $(this).addClass("active-tab");

        $(".js-tab-content.active-tab").removeClass("active-tab");
        tab.addClass("active-tab");
    });

    // вызов блока выбора кол-ва цветков
    $('.buy-block__number').click(function () {
        const $result = $(this).next('.mount-card');

        if ($result.is(':hidden')) {
            $(".mount-card.show").removeClass("show");
            $result.addClass('show');
            $result.fadeIn();

        } else {
            $result.removeClass('show');
            $result.fadeOut();
        }
    })
    // снятие класса show при убирании курсора с блока выбора кол-ва цветков mount-card
    $(".mount-card").mouseleave(function () {
        $(this).removeClass("show");
    });

    // снятие класса choose с выбранного кол-ва цветков при убирании курсора с блока выбора кол-ва цветков mount-card
    $(".mount-card").mouseleave(function () {
        $(this).find(".block-number__item").removeClass("choose");
    });

    // обработка события hover на активном элементе
    $(".phls__sub").hover(function () {
        $(".phls__sub.hover-active").removeClass("hover-active");
        $(this).addClass("hover-active");

    })
    // снятие класса hover-active с непросматриваемого раздела меню
    $('.page-header__sub-menu-list-wrapper').mouseleave(function () {
        $(this).parents('.page-header__item-section2').find('.phls__sub').removeClass('hover-active');
    });

    // снятие класса choose с готового кол-ва цветков при активном input
    $(".block-number__input").focus(function () {
        $(this).parents(".mount-card-wrapper").find(".block-number__item").removeClass("choose");
    });

    // закрытие окна выбора кол-ва цветков по нажатию на Выбрать
    $(".block-number__submit").on("click", function () {
        $(this).parents(".mount-card").removeClass("show");
        $(this).parents(".mount-card").find(".block-number__input").val("");

    })


    // снятие класса chosen-mount с элемента выбора кол-ва цветков в карточке товара
    $(".content-button").on("click", function () {
        $(this).parents(".main-content-description").find(".mount-menu__item").removeClass("chosen-mount");
    });

    // очистка поля ввода кол-ва цветков при нажатии В корзину в карточке товара
    $(".content-button").on("click", function () {
        $(this).parents(".main-content-description").find(".mount-menu__input").val("");
    });


    $(".sizes-menu__title").on("click", function () {

        //переключение положения псевдоэлемента на блоке sizes-menu
        $(this).toggleClass("mark-rotate-active");
        $(this).parents(".swiper-item2").siblings().find(".sizes-menu__title").removeClass("mark-rotate-active");

        //переключение положения псевдоэлемента на блоке sizes-menu в карточке товара
        $(this).parents(".similar-item").siblings().find(".sizes-menu__title").removeClass("mark-rotate-active");


        //скрытие и показ блока с размерами sizes-menu-tablet
        $(this).parents(".swiper-item2").find(".sizes-menu-tablet").toggleClass("sizes-menu-tablet-active");
        $(this).parents(".swiper-item2").siblings().find(".sizes-menu-tablet").removeClass("sizes-menu-tablet-active");

        //скрытие и показ блока с размерами sizes-menu-tablet в карточке товара
        $(this).parents(".similar-item").find(".sizes-menu-tablet--content").toggleClass("sizes-menu-tablet-active--content");
        $(this).parents(".similar-item").siblings().find(".sizes-menu-tablet--content").removeClass("sizes-menu-tablet-active--content");

    });

    // изменение цвета кнопок выбора кол-ва штук блок sizes-item-tablet
    $(".sizes-item-tablet").on("click", function () {
        $(this).toggleClass("sizes-item-tablet-active");
        $(this).siblings().removeClass("sizes-item-tablet-active");
    });

    //снятие активных классов выделения кнопок cart-button
    $(".cart-button2").on("click", function () {
        // $(this).parents(".buy-block2").find(".sizes-item-tablet").removeClass("sizes-item-tablet-active");
        $(this).parents(".swiper-item2").removeClass("swiper-item2-tablet");
        $(this).parents(".swiper-item2").find(".sizes-menu__title").removeClass("mark-rotate-active");
        $(this).parents(".buy-block2").find(".sizes-menu-tablet").removeClass("sizes-menu-tablet-active");
    });

    //открытие модального окна Купить в 1 клик
    $(".buy-button, .content-link").on("click", function (evt) {
        evt.preventDefault();
        $(".one-click").fadeIn(700);
        const thh = $(this),
            item1 = thh.parents('.swiper-item1'),
            item2 = thh.parents('.swiper-item2'),
            item3 = thh.parents('.card-main-content');
        let title = '',
            size = 0,
            count = 0;

        if (item1.length) {
            size = item1.find('.sizes-radio.active');
            count = item1.find('.add_to_cart_button');
            title += item1.find('.flower__title').text();
            title += ', цена: ' + item1.find('.flower__price').text();
        }

        if (item2.length) {
            size = item2.find('.sizes-radio.active');
            count = item2.find('.add_to_cart_button');
            title += item2.find('.flower__title2').text();
            title += ', цена: ' + item2.find('.flower__price2').text();
        }

        if (size.length) title += ', размер: ' + size.find('.sizes-menu__div').text() + ' ' + size.find('.popup-size').text();

        if (item3.length) {
            size = item3.find('.sizes-radio-content.active');
            count = item3.find('.add_to_cart_button');
            title += item3.find('.main-content-title').text();
            if (size.length) title += ', размер: ' + size.find('.sizes-menu__div-content').text() + ' ' + size.find('.popup-size-content').text();
            title += ', цена: ' + item3.find('.main-content-price').text();
        }

        if (count.length) title += ', кол-во: ' + count.attr('data-quantity');

        $('.one-click-input-product input').val(title);
    });

    //закрытие модального окна Купить в 1 клик
    $(".one-click__button").on("click", function () {
        $(".one-click").fadeOut(500);
    });

    // открытие меню бургер на мобильной версии
    $(".page-header__burgermenu").on("click", function (evt) {
        evt.preventDefault();
        $(".page-header__section-bottom-wrapper").toggleClass("mobile-menu-open");
        $(this).toggleClass("burger-rotate");
    })

    // отмена событий по умолчанию на пунктах меню
    $(".phls__sub").on("click", function (evt) {
        evt.preventDefault();
    });

    $(".page-header__sub-menu-link").on("click", function (evt) {
        // evt.preventDefault();
    });


    // прокрутка картинок по нажатию на arrow-button в моб. версии
    let $elWidth = $(".swiper-item1").outerWidth(true);
    let $box = $(".slider1");

    $(".arrow-button-special").on("click", function () {
        $box.stop().animate({
            scrollLeft: "+=" + ($elWidth * $(this).data("factor"))
        });
    });

    let $elWidth2 = $(".swiper-item1").outerWidth(true);
    let $box2 = $(".bestsellers-list");
    $(".arrow-button-bestsellers").on("click", function () {
        $box2.stop().animate({
            scrollLeft: "+=" + ($elWidth2 * $(this).data("factor"))
        });
    });

    let $elWidth3 = $(".swiper-item2").outerWidth(true);
    let $box3 = $(".slider2");
    $(".arrow-button-new").on("click", function () {
        $box3.stop().animate({
            scrollLeft: "+=" + ($elWidth3 * $(this).data("factor"))
        });
    });

    let $elWidth4 = $(".swiper-item3").outerWidth(true);
    let $box4 = $(".slider3");
    $(".arrow-button-seen").on("click", function () {
        $box4.stop().animate({
            scrollLeft: "+=" + ($elWidth4 * $(this).data("factor"))
        });
    });

    let $elWidth5 = $(".city-inst__item").outerWidth(true);
    let $box5 = $(".city-inst__photos");
    $(".arrow-button-inst").on("click", function () {
        $box5.stop().animate({
            scrollLeft: "+=" + ($elWidth5 * $(this).data("factor"))
        });
    });

    let $elWidth6 = $(".similar-item").outerWidth(true);
    let $box6 = $(".similar-items-list");
    $(".arrow-button-content").on("click", function () {
        $box6.stop().animate({
            scrollLeft: "+=" + ($elWidth6 * $(this).data("factor"))
        });
    });

    let $elWidth7 = $(".swiper-item2").outerWidth(true);
    let $box7 = $(".slider2-similar");
    $(".arrow-button-similar").on("click", function () {
        $box7.stop().animate({
            scrollLeft: "+=" + ($elWidth7 * $(this).data("factor"))

        });
    });

    // скрытие и показ блока Кол-во цветков
    $(".buy-block__number--short").on("click", function () {
        $(this).parents(".swiper-item1").find(".mount-card-mobile").toggleClass("mount-card-mobile--active");
        $(this).parents(".swiper-item1").siblings().find(".mount-card-mobile").removeClass("mount-card-mobile--active");
        $(this).toggleClass("short-rotate");
    });

    // выделение активного элемента выбора кол-ва штук
    $(".card-mobile__item").on("click", function () {
        $(this).toggleClass("card-mobile__item--active");
        $(this).siblings().removeClass("card-mobile__item--active");
        $(this).parents(".swiper-item1").siblings().find(".card-mobile__item").removeClass("card-mobile__item--active");

    });

    // снятие активного класса элемента выбора кол-ва штук по нажатию на В корзину
    $(".cart-button").on("click", function () {
        $(".swiper-item1").find(".card-mobile__item").removeClass("card-mobile__item--active");
        $(".swiper-item1").find(".mount-card-mobile").removeClass("mount-card-mobile--active");
    })


    // скрытие и показ блока Кол-во цветков в блоке bestsellers
    $(".buy-block__number--short").on("click", function () {
        $(this).parents(".flower-card--bestsellers").find(".mount-card-mobile").toggleClass("mount-card-mobile--active");
        $(this).parents(".flower-card--bestsellers").siblings().find(".mount-card-mobile").removeClass("mount-card-mobile--active");

    });

    // выделение активного элемента выбора кол-ва штук в блоке bestsellers
    $(".card-mobile__item").on("click", function () {
        $(this).parents(".flower-card--bestsellers").siblings().find(".card-mobile__item").removeClass("card-mobile__item--active");

    });

    // снятие активного класса элемента выбора кол-ва штук по нажатию на В корзину в блоке bestsellers
    $(".cart-button").on("click", function () {
        $(".flower-card--bestsellers").find(".card-mobile__item").removeClass("card-mobile__item--active");
        $(".flower-card--bestsellers").find(".mount-card-mobile").removeClass("mount-card-mobile--active");

    });

    // $("#lightgallery").lightGallery();

    // отмена действия по умолчанию для элемента меню footer-bouquet-item
    $(".footer-bouquet-item").on("click", function (evt) {
        evt.preventDefault();

    });

    $(window).on('resize', function () {
        if ($(window).width() < 768) {

            // $(".")

        }
    });


    $('.custom_add_to_cart').click(function (e) {
        e.preventDefault();
        const thh = $(this);
        // const id = thh.nextAll('input[name="product_id"]').val();
        // const addtocart = thh.nextAll('input[name="add-to-cart"]').val();
        const variationid = thh.nextAll('input[name="variation_id"]').val();
        let data = {
            product_id: variationid,
            // 'add-to-cart': addtocart,
            // variation_id: variationid,
            quantity: 1
        };
        thh.addClass('loading');
        $.post(wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'add_to_cart'), data, function (response) {

            if (!response) {
                return;
            }
            if (response.error) {
                // alert("Custom Massage ");
                thh.removeClass('loading');
                return;
            }
            if (response) {
                var url = woocommerce_params.wc_ajax_url;
                url = url.replace("%%endpoint%%", "get_refreshed_fragments");
                $.post(url, function (data, status) {
                    $(".woocommerce.widget_shopping_cart").html(data.fragments["div.widget_shopping_cart_content"]);
                    if (data.fragments) {
                        jQuery.each(data.fragments, function (key, value) {

                            jQuery(key).replaceWith(value);
                        });
                    }
                    jQuery("body").trigger("wc_fragments_refreshed");
                });
                thh.removeClass('loading').addClass('added').after(' <a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' +
                    wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>')
            }
        });
    });

    if ($('.product .variations').length) {
        function variationsChange() {
            let radio = $('.sizes-radio-content').eq(0);
            let sizeBlock = $('.pa-size-block');
            let diameterBlock = $('.pa-diameter-block');
            let selectSizes = $('#pa_size');
            let selectdiameters = $('#pa_diameter');
            let sizes = selectSizes.find('option');
            let diameters = selectdiameters.find('option');
            sizeBlock.html('');
            diameterBlock.html('');
            $.each(sizes, function (i, v) {
                if (i === 0) return true;
                v = $(v);
                let radioClone = radio.clone();
                let val = v.attr('value');
                let active = '';
                let valSize = val.split('-')[1];
                if (val === selectSizes.val()) active = 'active';
                radioClone.addClass(active).data('value', val).data('type', 'pa_size').find('.sizes-menu__div-content').text(v.text())
                if (valSize) {
                    radioClone.find('.popup-size-content').text(valSize + ' см');
                }
                sizeBlock.prepend(radioClone);
            });
            $.each(diameters, function (i, v) {
                if (i === 0) return true;
                v = $(v);
                let radioClone = radio.clone();
                let val = v.attr('value');
                let active = '';
                if (val === selectdiameters.val()) active = 'active';
                radioClone.addClass(active).data('value', val).data('type', 'pa_diameter').find('.sizes-menu__div-content').text(v.text())
                radioClone.find('.popup-size-content').text(v.text())
                diameterBlock.append(radioClone);
            });
            // radio-buttons для карточки товара
            $('.sizes-radio-content').on('click', function () {
                const ths = $(this);
                $('#' + ths.data('type')).val(ths.data('value'));
                $('#' + ths.data('type')).trigger('change');
                let price = ths.parents('.main-content-description').find('.woocommerce-variation-price .woocommerce-Price-amount').text();
                if (price) price = price.slice(0, -1);
                $('.main-content-price').html(price + ' ₽');
                ths.siblings('.sizes-radio-content').removeClass('active');
                ths.addClass('active');
                // variationsChange();
            });
        }
        variationsChange();
    }
    if ($('.mount-menu-content')) {
        const price = $('.main-content-price').text().replace(/[^\d;]/g, '').slice(0, -2);
        let button = $('.main-contents-buttons .add_to_cart_button').eq(0);
        // переключения цвета на кнопках кол-ва штук в блоке mount-card в карточке товара
        $(".mount-menu__item").on("click", function () {
            // $(".mount-menu__item").removeClass("chosen-mount");
            let ths = $(this);
            ths.toggleClass("chosen-mount");
            ths.siblings().removeClass("chosen-mount");
            ths.parents(".mount-menu-content").find(".mount-menu__input").val("");
            button.attr('data-quantity', ths.data('count'));
            $('.main-content-price').html(price * ths.data('count') + ',00 ₽');
        })

        // снятие класса chosen-mount с готового кол-ва цветков при активном input в карточке товара
        $(".mount-menu__input").change(function () {
            let ths = $(this);
            let val = ths.val().replace(/[^\d;]/g, '');
            ths.parents(".mount-menu-content").find(".mount-menu__item").removeClass("chosen-mount");
            button.attr('data-quantity', val)
            $('.main-content-price').html(price * val + ',00 ₽');
        });
    }
    if ($('.swiper-item1,.swiper-item2').length) {
        $.each($('.swiper-item1,.swiper-item2'), function (i, v) {
            const p = $(v);
            if (p.find('.variations').length) {
                let radio = p.find('.sizes-radio').eq(0);
                let sizeBlock = p.find('.pa-size-block');
                let selectSizes = p.find('#pa_size');
                let sizes = selectSizes.find('option');
                sizeBlock.html('');
                $.each(sizes, function (i, v) {
                    if (i === 0) return true;
                    v = $(v);
                    let radioClone = radio.clone();
                    let val = v.attr('value');
                    let active = '';
                    let valSize = val.split('-')[1];
                    if (val === selectSizes.val()) active = 'active';
                    radioClone.addClass(active).data('value', val).data('type', 'pa_size').find('.sizes-menu__div').text(v.text());
                    if (valSize) {
                        radioClone.find('.popup-size').text(valSize + ' см');
                    }
                    sizeBlock.prepend(radioClone);
                });
                // radio-buttons для карточки товара
                p.find('.sizes-radio').on('click', function () {
                    const ths = $(this);
                    p.find('#' + ths.data('type')).val(ths.data('value'));
                    p.find('#' + ths.data('type')).trigger('change');
                    let price = p.find('.woocommerce-variation-price .woocommerce-Price-amount').text();
                    if (price) price = price.slice(0, -1);
                    p.find('.flower__pricej').html(price + ' ₽');
                    ths.siblings('.sizes-radio').removeClass('active');
                    ths.addClass('active');
                    // variationsChange();
                });
            }
            if (p.find('.mount-card')) {
                const price = p.find('.flower__pricej').text().replace(/[^\d;]/g, '').slice(0, -2);
                let button = p.find('.add_to_cart_button').eq(0);
                // переключения цвета на кнопках кол-ва штук в блоке mount-card в карточке товара
                p.find(".block-number__item").on("click", function () {
                    // $(".mount-menu__item").removeClass("chosen-mount");
                    let ths = $(this);
                    ths.addClass("choose");
                    ths.siblings().removeClass("choose");
                    ths.parents(".mount-card").find(".block-number__input").val("");
                    button.attr('data-quantity', ths.data('count'));
                    p.find('.flower__pricej').html(price * ths.data('count') + ',00 ₽');
                })

                // снятие класса chosen-mount с готового кол-ва цветков при активном input в карточке товара
                p.find(".block-number__input").change(function () {
                    let ths = $(this);
                    let val = ths.val().replace(/[^\d;]/g, '');
                    ths.parents(".mount-card").find(".block-number__item").removeClass("choose");
                    button.attr('data-quantity', val)
                    p.find('.flower__pricej').html(price * val + ',00 ₽');
                });
            }
        });
    }

    // показ скрытой формы в checkout
    $(".replase-check").on("click", function () {
        if ($(".delivery-checkbox--1").is(":checked")) {
            $(".delivery-form--hidden").slideDown(500);
        } else {
            $(".delivery-form--hidden").slideUp(400);
        }
    });

    // выпадающие списки выбора
    $('.select').on('click', '.select__head', function () {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).next().fadeOut(300);
        } else {
            $('.select__head').removeClass('open');
            $('.select__list').fadeOut(300);
            $(this).addClass('open');
            $(this).next().fadeIn(300);
        }
    });

    $('.select').on('click', '.select__item', function () {
        $('.select__head').removeClass('open');
        let thh = $(this);
        let type = thh.data('type');
        let price = thh.data('price');
        let mainBlock = thh.parents('.select');
        let inp = mainBlock.find('.select__input');
        thh.parent().fadeOut(300);
        mainBlock.find('.select__head').text(thh.text());
        inp.val(thh.text());
        inp.trigger('input');
        if (inp.attr('name') === 'paytype') {
            $('input[name="payment_method"][value="' + type + '"]').click();
        }
        if (inp.attr('name') === 'delivery') {
            $('input[name="shipping_method[0]"]').val(type);
            jQuery('body').trigger('update_checkout');
            setTimeout(() => {
                $('.shipping-price').text(price);
            }, 500);
        }
        if (type === 'local_pickup:2') {
            $('.dif-address,.dif-time,.dif-data').addClass('events-none');
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".cb-customer").offset().top
            }, 1000);
        }
        else {
            if (type === 'flat_rate:3') {
                $('.dif-time .select__head').css('display', 'block');
                $('.dif-time .time-block').css('display', 'none');
                $('.dif-time .select__head').html('с 09:00 до 11:30');
                $('.dif-time .select__input').val('с 09:00 до 11:30');
                $('.dif-time .select__list').eq(0).html('<li class="select__item">С 9:00 до 11:30</li><li class="select__item">С 11:30 до 14:00</li><li class="select__item">С 14:00 до 16:30</li><li class="select__item">С 16:30 до 19:00</li><li class="select__item">С 19:00 до 21:30</li>');
            } else if (type === 'flat_rate:4') {
                $('.dif-time .select__head').css('display', 'block');
                $('.dif-time .time-block').css('display', 'none');
                $('.dif-time .select__head').html('с 09:00 до 12:00');
                $('.dif-time .select__input').val('с 09:00 до 12:00');
                $('.dif-time .select__list').eq(0).html('<li class="select__item">С 9:00 до 12:00</li><li class="select__item">С 12:00 до 15:00</li><li class="select__item">С 15:00 до 18:00</li><li class="select__item">С 18:00 до 21:00</li>');
            } else {
                $('.dif-time .select__head').css('display', 'none');
                $('.dif-time .time-block').css('display', 'block');
            }
            $('.dif-address,.dif-time,.dif-data').removeClass('events-none');
        }
    });

    $(document).click(function (e) {
        if (!$(e.target).closest('.select').length) {
            $('.select__head').removeClass('open');
            $('.select__list').fadeOut(300);
        }
    });

    if ($('.order-form').length) {
        var name = $('#billing_first_name').val(),
            phone = $('#billing_phone').val(),
            email = $('#billing_email').val(),
            address1 = $('#billing_address_1').val();
        $('.order-form input[name="billing_first_name"]').val(name);
        $('.order-form input[name="billing_phone"]').val(phone);
        $('.order-form input[name="billing_email"]').val(email);
        $('.order-form textarea[name="billing_address_1"]').val(address1);
        $('.order-form input[name="shipping_method[0]"][value="local_pickup:2"]').click();
        $('input[name="data"]').datepicker({
            onSelect: function () {
                $('input[name="delivery_time"]').trigger('input');
            }
        });
        $.mask.definitions['Z'] = "[01234569]";

        $('.order-form input[name="billing_phone"]').mask("7 (Z99) 999-9999");
        $('.order-form input[name="billing_phone"]').on('change', function (event) {
            var thh = $(this),
                to = thh.attr('name');
            $('#' + to).val(thh.val());
        })

        $('#datepicker-time').on('input', function (e) {
            $('.dif-time .select__input').val($(this).val());
        });
    }

    $('input[name="billing_first_name"],input[name="billing_phone"],input[name="billing_email"],textarea[name="billing_address_1"]').on('input', function (event) {
        event.preventDefault();
        var thh = $(this),
            to = thh.attr('name');
        $('#' + to).val(thh.val());
    });
    $('input[name="delivery_time"],input[name="data"],textarea[name="comment"],input[name="recipient_name"],input[name="recipient_contact"],textarea[name="recipient_comment"],#dc-2').on('input', function (event) {
        event.preventDefault();
        var thh = $(this),
            time = $('input[name="delivery_time"]').val(),
            timeText = time ? 'Время доставки: ' + time : '',
            data = $('input[name="data"]').val(),
            dataText = data ? 'Дата доставки: ' + data : '',
            comment = $('textarea[name="comment"]').val(),
            commentText = comment ? 'Комментарии и текст пожеланий на открытке: ' + comment : '',
            commentAll = timeText + '\n' + dataText + '\n' + commentText + '\n';
        if ($('#dc-1').is(":checked")) {
            commentAll += 'Получатель другой человек \n';
            commentAll += 'Имя получателя: ' + $('input[name="recipient_name"]').val() + '\n';
            commentAll += 'Контакты получателя: ' + $('input[name="recipient_contact"]').val() + '\n';
            commentAll += 'Комментарий для получателя: ' + $('textarea[name="recipient_comment"]').val() + '\n';
        }
        if ($('#dc-2').is(":checked")) {
            commentAll += 'Анонимно: ' + ($('#dc-2').val() ? 'да' : 'нет');
        }
        $('#order_comments').val(commentAll);
    });
    $('textarea[name="billing_address_1"]').on('input', function (event) {
        event.preventDefault();
        var thh = $(this),
            address = $('textarea[name="billing_address_1"]').val();
        $('#billing_address_1').val(address);
    });

    $('.checkout-button-order').on('click', function (event) {
        event.preventDefault();
        var err = 0;
        if (!$('input[name="agree"]').is(":checked")) {
            err = 1;
        }
        if (err === 0) {
            $('.woocommerce-checkout button[name="woocommerce_checkout_place_order"]').trigger('click');
        }

    });

});

