<?php

/**
 * Template used to display post content on single pages.
 *
 * @package storefront
 */

global $product;
$tags = get_the_terms($product->id, 'product_tag');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="content-single-cover">
        <div class="main-content page-wrapper">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a></li>
                <!-- <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Букеты</a></li> -->
                <li class="breadcrumbs__item breadcrumbs__item--current">
                    <?php echo $product->get_categories() ?>
                </li>
            </ul>

            <div class="content-single">
                <div class="card-main-content">

                    <div class="content-single-slider">
                        <div class="main-content-img-wrapper">
                            <!-- <picture>
    						<source type="image/webp" srcset="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/webp/product-page-main-01.webp">
                            <img class="main-content-img" src="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/comp/product-page-main-01.jpg" alt="Фотография товара">
                            </picture> -->

                            <!-- <div class="main-content-controls">
                                <button class="main-content-nav main-content-left">
                                    <svg width="22" height="40" viewBox="0 0 22 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g opacity="0.6">
                                            <path d="M8.6217e-07 19.7241L19.25 1.20206e-07L22 0L22 2.81773L5.5 19.7241L22 36.6305L22 39.4483L19.25 39.4483L8.6217e-07 19.7241Z" fill="white" />
                                        </g>
                                    </svg>
                                </button>
                                <button class="main-content-nav main-content-right">
                                    <svg width="22" height="40" viewBox="0 0 22 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g opacity="0.6">
                                            <path d="M22 20.2759L2.75 40L0 40L-1.23167e-07 37.1823L16.5 20.2759L-1.60117e-06 3.36945L-1.72434e-06 0.55172L2.75 0.55172L22 20.2759Z" fill="white" />
                                    </svg>

                                </button>
                            </div> -->


                        </div>
                        <!-- кнопки переключения -->

                        <!-- точки переключения -->
                        <div class="card-small-img__list fotorama" data-thumbmargin="10" data-allowfullscreen="true" data-arrows="always" data-thumbwidth="100%" id="lightgallery" data-nav="thumbs">
                            <?php
                            foreach ($product->get_gallery_image_ids() as $img) {
                                echo wp_get_attachment_image($img, 'full');
                            } ?>
                        </div>
                        <?php //woocommerce_show_product_images(); 
                        ?>
                        <!-- <div id="lightgallery">
<a href="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-main-01.jpg"><img src="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-small-01.jpg" alt=""></a>
<a href="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-main-01.jpg"><img src="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-small-02.jpg" alt=""></a>
<a href="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-small-03.jpg"><img src="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-small-03.jpg" alt=""></a>
</div> -->

                        <!-- <div class="fotorama" data-nav="thumbs">
<img src="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-small-01.jpg" alt=""></>
<img src="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-small-02.jpg" alt=""></>
<img src="/wp-content/themes/cityflo/assets/images/loaded/comp/product-page-small-03.jpg" alt=""></>
</div> -->

                    </div>

                    <div class="main-content-description">
                        <div class="description-spacing">
                            <div class="main-content-top-wrapper">
                                <h1 class="main-content-title"><?php echo $product->get_title(); ?></h1>
                                <?php if ($tags) : ?>
                                    <div class="adds-content">
                                        <?php
                                        foreach ($tags as $tag) :
                                            get_template_part('tags/' . $tag->slug);
                                        endforeach;
                                        ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <span class="main-content-price"><?php echo good_product_price(); ?>,00 &#8381;</span>
                            <?php if ($product->is_type('variable')): ?>
                                <div class="sizes-menu-content">
                                    <span class="sizes-menu__title-content">Размер:</span>
                                    <div class="radio-wrapper-content pa-size-block">
                                        <div class="sizes-radio-content">
                                            <div class="sizes-menu__div-content"></div>
                                            <div class="popup-size-content"></div>
                                        </div>
                                    </div>
                                    <!-- <span class="sizes-menu__title-content">Димаетр букета:</span>
                                    <div class="radio-wrapper-content pa-diameter-block">
                                    </div> -->
                                </div>
                            <?php endif; ?>
                            <?php if (!$product->is_type('variable')): ?>
                                <div class="mount-menu-content">
                                    <span class="mount-menu__title-content">Выберите количество цветков:</span>
                                    <ul class="mount-menu__list">
                                        <li data-count="11" class="mount-menu__item">11 шт</li>
                                        <li data-count="17" class="mount-menu__item">17 шт</li>
                                        <li data-count="25" class="mount-menu__item">25 шт</li>
                                        <li data-count="37" class="mount-menu__item">37 шт</li>
                                    </ul>
                                    <span class="mount-menu__or">или</span>
                                    <input class="mount-menu__input" type="text" placeholder="Введите свой вариант">
                                </div>
                            <?php endif; ?>
                            <div class="main-tabs">
                                <ul class="main-tabs-header">
                                    <li class="main-tabs-header__item js-tab-trigger active-tab" data-tab="1">Состав</li>
                                    <li class="main-tabs-header__item js-tab-trigger" data-tab="2">Доставка</li>
                                    <li class="main-tabs-header__item js-tab-trigger" data-tab="3">Оплата</li>
                                    <li class="main-tabs-header__item js-tab-trigger" data-tab="4">Комплектация</li>
                                </ul>

                                <div class="main-tabs-content">
                                    <div class="main-tabs-content__item js-tab-content active-tab" data-tab="1">
                                        <div class="tab-content-wrapper">
                                            <?php echo wpautop($product->get_description()); ?>
                                        </div>
                                    </div>
                                    <div class="main-tabs-content__item js-tab-content" data-tab="2">
                                        <div class="tab-content-wrapper">
                                            <p>
                                                Стоимость доставки:
                                            </p>
                                            <br>
                                            <p>
                                                В пределах МКАД (Зона №1) - 500 рублей
                                            </p>
                                            <p>
                                                За МКАД (Зона №2) - 850 рублей
                                            </p>
                                            <p>
                                                Доставка далее Зоны №2 - оговаривается отдельно с администратором
                                            </p>
                                            <br>
                                            <p>
                                                Доставка к точному времени интервал доставки 15-20 минут:
                                            </p>
                                            <br>
                                            <p>
                                                В пределах МКАД (Зона №1) - 1000 рублей
                                            </p>
                                            <p>
                                                За МКАД (Зона №2) - 1500 рублей
                                            </p>
                                            <p>
                                                Далее Зоны №2 - оговаривается отдельно с администратором
                                            </p>
                                            <p>
                                                <a href="/shipping-payment/">Подробнее о доставке</a>
                                            </p>

                                        </div>
                                    </div>
                                    <div class="main-tabs-content__item js-tab-content" data-tab="3">
                                        <div class="tab-content-wrapper">
                                            <p>
                                                Банковской картой Visa/Mastercard/МИР - при оформлении заказа на сайте
                                            </p>
                                            <p>
                                                Наличными - в нашем салоне
                                            </p>
                                            <p>
                                                <a href="/shipping-payment/">Подробнее об оплате</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="main-tabs-content__item js-tab-content" data-tab="4">
                                        <div class="tab-content-wrapper">
                                            <p>
                                                Все наши букеты мы упаковываем в фирменную коробку для сохранности букета при транспортировке. Букет транспортируется в аквапаке, поэтому вы можете не переживать, что букет будет ехать без воды.
                                            </p>
                                            <br>
                                            <p>
                                                Такая упаковка позволяет надежно защитить цветы в дождливую и ветреную погоду.
                                            </p>
                                            <br>
                                            <p>
                                                Все наши цветочные композиции в корзинках, ящичках и шляпных коробочках мы надежно упаковываем в прозрачную пленку (крафтовую бумагу) и, при необходимости, кладем их в крафтовый пакет или специальную коробку для сохранности при транспортировке.
                                            </p>
                                            <br>
                                            <p>
                                                Вы можете не переживать за сохранность вашего букета или композиции при транспортировке, т.к. в плохую погоду, особенно в зимний период времени, все наши букеты и композиции мы отправляем на такси.
                                            </p>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div class="main-contents-buttons">
                                <?php iconic_template_loop_add_to_cart(); ?>
                                <a href="#" class="content-link">Купить в 1 клик</a>
                            </div>


                        </div>


                    </div>

                </div>

                <h2 class="similar-items-title">Похожие букеты</h2>

                <div class="slider2-block similar-single-block">
                    <div class="slider2 slider2-similar">
                        <?php
                        $args = array(
                            'limit' => 4,
                            'orderby'          => 'rand',
                            'post_status'    => 'publish',
                        );
                        $products = wc_get_products($args);
                        foreach ($products as $product) :
                            get_template_part('item-small');
                        endforeach;
                        ?>

                    </div>

                    <button class="arrow-button arrow-button-similar arrow-left" data-factor="-1">
                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="40" height="40" rx="20" transform="matrix(-1 0 0 1 40 0)" fill="#F8F8F8" />
                            <path opacity="0.5" d="M15.0776 20.8264L20.4774 26.416C20.8209 26.7718 21.3779 26.7718 21.7212 26.416C22.0645 26.0606 22.0645 25.4841 21.7212 25.1287L16.9432 20.1828L21.721 15.237C22.0644 14.8814 22.0644 14.305 21.721 13.9495C21.3777 13.594 20.8208 13.594 20.4773 13.9495L15.0775 19.5393C14.9058 19.7171 14.8201 19.9498 14.8201 20.1827C14.8201 20.4158 14.906 20.6487 15.0776 20.8264Z" fill="black" />
                        </svg>
                    </button>

                    <button class="arrow-button arrow-button-similar arrow-right" data-factor="1">
                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="40" height="40" rx="20" fill="#F8F8F8" />
                            <path opacity="0.5" d="M24.9224 20.8264L19.5226 26.416C19.1791 26.7718 18.6221 26.7718 18.2788 26.416C17.9355 26.0606 17.9355 25.4841 18.2788 25.1287L23.0568 20.1828L18.279 15.237C17.9356 14.8814 17.9356 14.305 18.279 13.9495C18.6223 13.594 19.1792 13.594 19.5227 13.9495L24.9225 19.5393C25.0942 19.7171 25.1799 19.9498 25.1799 20.1827C25.1799 20.4158 25.094 20.6487 24.9224 20.8264Z" fill="black" />
                        </svg>
                    </button>
                </div>


            </div>

        </div>
    </div>

    <div class="one-click">
        <div class="one-click__wrapper">

            <button type="button" class="one-click__button">Закрыть<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path width="27" height="27" opacity="0.5" fill-rule="evenodd" clip-rule="evenodd" d="M9.99972 7.99338L17.5775 0.415598C18.1316 -0.138533 19.0301 -0.138533 19.5842 0.415598C20.1383 0.969729 20.1383 1.86815 19.5842 2.42228L12.0064 10.0001L19.584 17.5777C20.1382 18.1318 20.1382 19.0303 19.584 19.5844C19.0299 20.1385 18.1315 20.1385 17.5774 19.5844L9.99972 12.0068L2.42243 19.584C1.8683 20.1382 0.969877 20.1382 0.415746 19.584C-0.138385 19.0299 -0.138385 18.1315 0.415746 17.5774L7.99303 10.0001L0.415598 2.42264C-0.138533 1.86851 -0.138533 0.970081 0.415598 0.41595C0.969729 -0.138181 1.86815 -0.138181 2.42228 0.415951L9.99972 7.99338Z" fill="black" />
                </svg></button>
            <h2 class="one-click__title">Купить в 1 клик</h2>

            <!-- <form class="one-click__form" method="POST" action="#">
                <div class="user user-wrapper">
                    <p class="user__cell">
                        <label class="user__label" for="user-name" name="your-name">Ваше имя</label>
                        <input class="user__input" id="user-name" type="text" placeholder="Введите данные">
                    </p>
                    <p class="user__cell">
                        <label class="user__label" for="recipient" name="recipient-name">Имя получателя</label>
                        <input class="user__input" id="recipient" type="text" placeholder="Введите данные">
                    </p>
                    <p class="user__cell">
                        <label class="user__label" for="user-tel" name="telephone">Телефон</label>
                        <input class="user__input" id="user-tel" type="text" placeholder="+7 (___) ___-__-__">
                    </p>
                    <p class="user__cell">
                        <label class="user__label" for="user-adress" name="your-adress">Адрес доставки</label>
                        <input class="user__input" id="user-adress" type="text" placeholder="Введите данные">
                    </p>
                    <p class="user__cell">
                        <label class="user__label" for="user-mail" name="email">Email</label>
                        <input class="user__input" id="user-mail" type="text" placeholder="mail@info.ru">
                    </p>
                    <p class="user__cell">
                        <label class="user__label" for="user-coment" name="coments">Ваши коментарии</label>
                        <input class="user__input" id="user-coment" type="text" placeholder="Введите данные">
                    </p>
                </div>
                <label class="agreement" for="user-agree">
                    <input id="user-agree" type="checkbox" name="agree" class="visually-hidden">
                    <span class="checkbox-indicator"></span>
                    Я согласен на <a class="agreement__process-link" href="#">обработку персональных данных</a>

                </label>
                <button class="user__button-submit cart-button button" type="submit">Отправить</button>

            </form> -->
            <?php
                echo do_shortcode('[wpforms id="150"]');
            ?>
        </div>

    </div>

</article><!-- #post-## -->