<?php

/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
	exit;
}

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
	echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout woocommerce-checkout-none" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

	<?php if ($checkout->get_checkout_fields()) : ?>

		<?php do_action('woocommerce_checkout_before_customer_details'); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action('woocommerce_checkout_billing'); ?>
			</div>

			<div class="col-2">
				<?php do_action('woocommerce_checkout_shipping'); ?>
			</div>
		</div>

		<?php do_action('woocommerce_checkout_after_customer_details'); ?>

	<?php endif; ?>

	<?php do_action('woocommerce_checkout_before_order_review_heading'); ?>

	<h3 id="order_review_heading"><?php esc_html_e('Your order', 'woocommerce'); ?></h3>

	<?php do_action('woocommerce_checkout_before_order_review'); ?>

	<?php do_action('woocommerce_checkout_after_order_review'); ?>

	<?php do_action( 'woocommerce_checkout_order_review' ); ?>


</form>

<div class="order-form">
	<div class="of-left">

		<div class="connection-block connection-block--hidden">
			<div class="connection-block__number">1</div>
		</div>

		<div class="checkout-block">
			<h2 class="checkout-block__heading">Доставка</h2>
			<a href="/shipping-payment/" target="_blank" class="checkout-block__par">Условия доставки</a>
			<form class="delivery-form" action="">
				<div class="delivery-form__item select">
					<label class="delivery-form__title" for="">Способ доставки</label>
					<input class="select__input" name="delivery" type="hidden">
					<div class="select__head">Самовывоз - 0 руб.</div>
					<ul class="select__list">
						<li class="select__item" data-price="0" data-type="local_pickup:2">Самовывоз - 0 руб.</li>
						<li class="select__item" data-price="500" data-type="flat_rate:3">Доставка в пределах МКАД (Зона №1) - 500 руб.</li>
						<li class="select__item" data-price="850" data-type="flat_rate:4">Доставка за МКАД (Зона №2) - 850 руб.</li>
						<!-- <li class="select__item" data-price="1200" data-type="flat_rate:5">Курьер за МКАД (1200 руб.)</li> -->
						<li class="select__item" data-price="1000" data-type="flat_rate:6">Доставка к точному времени в пределах МКАД (Зона №1) - 1000 руб.</li>
						<li class="select__item" data-price="1500" data-type="flat_rate:7">Доставка к точному времени за МКАД (Зона №2) - 1500 руб.</li>
					<?php
					/*
						date_default_timezone_set('Europe/Moscow');
						if (date("n") == '3' and intval(date("j")) <= 8) :
					?>
							<li class="select__item" data-price="500" data-type="flat_rate:8">Курьер в пределах МКАД (Зона №1) - 500 руб.</li>
							<li class="select__item" data-price="700" data-type="flat_rate:9">За МКАД (Зона №2) - 700 руб.</li>
					<?php 
						else : 
					?>
							<li class="select__item" data-price="350" data-type="flat_rate:3">Доставка в пределах МКАД (Зона №1) - 500 руб.</li>
							<li class="select__item" data-price="650" data-type="flat_rate:4">Доставка за МКАД (Зона №2) - 850 руб.</li>
							<!-- <li class="select__item" data-price="1200" data-type="flat_rate:5">Курьер за МКАД (1200 руб.)</li> -->
							<li class="select__item" data-price="950" data-type="flat_rate:6">Доставка к точному времени в пределах МКАД (Зона №1) - 1000 руб.</li>
							<li class="select__item" data-price="1450" data-type="flat_rate:7">Доставка к точному времени за МКАД (Зона №2) - 1500 руб.</li>
							
					<?php
						endif; 
						*/
					?>
					</ul>
				</div>

				<div class="delivery-form__item select dif-time events-none">
					<label class="delivery-form__title" for="">Время доставки</label>
					<input class="select__input" name="delivery_time" data-to="Время доставки" type="hidden" value="с 09:00 до 11:30">
					<div class="select__head">с 09:00 до 11:30</div>
					<ul class="select__list"></ul>
					<div class="time-block">
						<input type="text" name="time" data-to="Время доставки" placeholder="Укажите время" id="datepicker-time">
						<span>Интервал доставки 15-20 минут</span>
					</div>
				</div>

				<div class="delivery-form__item dif-data events-none">
					<label class="delivery-form__title" for="">Дата доставки</label>
					<input type="text" name="data" data-to="Дата доставки" id="datepicker">
				</div>

				<div class="delivery-form__item dif-address events-none">
					<span class="delivery-form__title">Адрес доставки</span>
					<textarea class="delivery-form__area" name="billing_address_1" id="" cols="30" rows="10"></textarea>
				</div>
			</form>
		</div>

		<div class="connection-block cb-customer">
			<div class="connection-block__number">2</div>
		</div>

		<div class="checkout-block">
			<h2 class="checkout-block__heading">Покупатель</h2>
			<form class="delivery-form" action="">
				<div class="delivery-form__item">
					<label class="delivery-form__title" for="">Ваше имя</label>
					<input class="delivery-form__info" name="billing_first_name" type="text">
				</div>
				<div class="delivery-form__item">
					<label class="delivery-form__title" for="">Ваш телефон</label>
					<input class="delivery-form__info" data-to="billing_phone" name="billing_phone" type="text">
				</div>
				<div class="delivery-form__item">
					<label class="delivery-form__title" for="">E-mail</label>
					<input class="delivery-form__info" name="billing_email" type="text">
				</div>
				
				<div class="delivery-form__item">
					<span class="delivery-form__title">Комментарии и текст ваших пожеланий на открытке</span>
					<textarea class="delivery-form__area" name="comment" data-to="Комментарии и текст ваших пожеланий на открытке" id="" cols="30" rows="10"></textarea>
				</div>
			</form>
		</div>

		<div class="connection-block">
			<div class="connection-block__number">3</div>
		</div>
		
		<div class="checkout-block">
			<h2 class="checkout-block__heading">Получатель</h2>
			<div class="delivery-check-block">
				<div class="c-block-1">
					<label class="delivery-checkbox replase-check" for="dc-1">
						<input class="delivery-checkbox--1 visually-hidden" id="dc-1" data-to="Получатель другой человек" type="checkbox">
						<span class="cb-i"></span>
						Получатель другой человек
					</label>
				</div>
				<form class="delivery-form delivery-form--hidden" action="">
					<div class="delivery-form__item">
						<label class="delivery-form__title" for="">Имя</label>
						<input class="delivery-form__info" name="recipient_name" data-to="Имя получателя" type="text">
					</div>
					<div class="delivery-form__item">
						<label class="delivery-form__title" for="">Контактная информация</label>
						<input class="delivery-form__info" name="recipient_contact" data-to="Контактная информация получателя" type="text">
					</div>
					<div class="delivery-form__item">
						<span class="delivery-form__title">Коментарии</span>
						<textarea class="delivery-form__area" name="recipient_comment" id="" data-to="Коментарии" cols="30" rows="10"></textarea>
					</div>					
				</form>
				<div class="c-block-2">
					<label class="delivery-checkbox" for="dc-2">
						<input class="delivery-checkbox--2 visually-hidden" data-to="Анонимно" name="recipient_anon" id="dc-2" type="checkbox">
						<span class="cb-i"></span>
						Анонимно (получатель не узнает, от кого букет)
					</label>
				</div>
			</div>
		</div>

		<div class="connection-block">
			<div class="connection-block__number">4</div>
		</div>

		<div class="checkout-block checkout-block--mb">
			<h2 class="checkout-block__heading">Оплата</h2>
			<div class="delivery-form__item select">
					<label class="delivery-form__title" for="">Способ оплаты</label>
					<input class="select__input" name="paytype" type="hidden">
					<div class="select__head">Оплата банковской картой онлайн</div>
					<ul class="select__list">
						<li class="select__item" data-type="ym_api_epl">Оплата банковской картой онлайн</li>
						<li class="select__item" data-type="cod">Самовывоз (ул.Горбунова, д. 2, стр. 3)</li>
					</ul>
				</div>
		</div>

		<div class="checkout-block checkout-agree">

		<label class="delivery-checkbox" for="dc-3">
			<input class="delivery-checkbox--3 visually-hidden" name="agree" checked id="dc-3" type="checkbox">
			<span class="cb-i"></span>
			Я согласен на <a href="#">обработку персональных данных</a>
		</label>

		</div>

		<div class="checkout-button-order">Оформить заказ</div>

	</div>
	<div class="of-right">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>
</div>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>