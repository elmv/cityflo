<?php

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cityflo
 */

?>

<main class="main-index">
	<h1 class="visually-hidden">City Flowers. Городские цветы</h1>

	<section class="benefits">
		<ul class="benefits__list page-wrapper">
			<li class="benefits__item benefits__item--position benefits__item--icon1">Букеты от 1490 рублей</li>
			<li class="benefits__item benefits__item--position benefits__item--icon2">Упаковка букета <br> и открытка в подарок</li>
			<li class="benefits__item benefits__item--position benefits__item--icon3">Доставим букет в воде. <br> Цветы останутся свежими</li>
			<li class="benefits__item benefits__item--position benefits__item--icon4">От 3000 рублей <br> одноразовая ваза и коробка - <br> переноска в подарок</li>
			<li class="benefits__item benefits__item--position benefits__item--icon5">Накопительная <br> система скидок</li>
		</ul>
	</section>

	<article class="collection-advertising">
		<h2 class="special-collection__heading">К 1 сентября мы разработали <br> для вас специальную коллекцию!</h2>
		<a href="#" class="special-collection__button button1">Смотреть все</a>
	</article>

	<section class="special-collection page-wrapper">


		<!-- Здесь будет слайдер 1 -->

		<div class="slider1-block">
			<div class="slider1">
				<!-- Slides -->
				<?php
				$args = array(
					'tag' => array('15', 'lightning', 'new'),
				);
				$products = wc_get_products($args);
				foreach ($products as $product) :
					$tags = get_the_terms($product->id, 'product_tag');
				?>
					<div class="swiper-item1 swiper-item1--margin">
						<div class="flower-picture">
							<a href="<?php echo $product->get_permalink(); ?>">
								<?php echo $product->get_image('woocommerce_thumbnail', ['class' => 'flower-img']); ?>
							</a>
							<?php if ($tags) : ?>
							<div class="adds adds--slider1">
								<?php
								foreach ($tags as $tag) :
									get_template_part('tags/' . $tag->slug);
								endforeach;
								?>
							</div>
							<?php endif; ?>
						</div>

						<div class="buy-block">
							<div class="wrapper-top">
								<div class="wrapper-left">
									<p class="flower__title"><?php echo $product->get_title(); ?></p>
									<span class="flower__price"><?php echo $product->get_price(); ?>,00 &#8381;</span>
								</div>
								<!-- <span class="buy-block__number">Выбрать кол - во <br> цветков</span>
								<div class="mount-card">
									<div class="mount-card-wrapper">
										<ul class="block-number">
											<li class="block-number__item">11 шт</li>
											<li class="block-number__item">17 шт</li>
											<li class="block-number__item">25 шт</li>
											<li class="block-number__item">37 шт</li>
										</ul>
										<span class="block-number__text">или введите свой вариант:</span>
										<div class="enter-wrapper">
											<input class="block-number__input" id="input-number" type="text" placeholder="Введите число">
											<button class="block-number__submit button" type="submit">Выбрать</button>

										</div>
									</div>
								</div>
								<span class="buy-block__number--short">Кол - во цветков</span> -->
								<!-- <div class="sizes-menu">

									<span class="sizes-menu__title">Размер букета:</span>
									<div class="radio-wrapper">
										<div class="sizes-radio sizes-radio--s">
											<div class="sizes-menu__div">S</div>
											<div class="popup-size">30 см</div>
										</div>

										<div class="sizes-radio sizes-radio--m">
											<div class="sizes-menu__div">M</div>
											<div class="popup-size">45 см</div>

										</div>

										<div class="sizes-radio sizes-radio--l">
											<div class="sizes-menu__div">L</div>
											<div class="popup-size">65 см</div>
										</div>
									</div>


								</div> -->

							</div>


							<!-- <div class="mount-card-mobile">
								<ul class="card-mobile__list">
									<li class="card-mobile__item">11 шт</li>
									<li class="card-mobile__item">17 шт</li>
									<li class="card-mobile__item">25 шт</li>
									<li class="card-mobile__item">37 шт</li>
								</ul>
							</div> -->

							<div class="wrapper-bottom">
								<?php woocommerce_template_loop_add_to_cart();?>
								<a href="#" class="buy-button">Купить в 1 клик</a>
							</div>
						</div>
					</div>
				<?php
				endforeach;
				?>
			</div>
			<!--slider1 end-->
		</div>
		<!--slider1 end-->

		<button class="arrow-button arrow-button-special arrow-left" data-factor="-1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" transform="matrix(-1 0 0 1 40 0)" fill="#F8F8F8" />
				<path opacity="0.5" d="M15.0776 20.8264L20.4774 26.416C20.8209 26.7718 21.3779 26.7718 21.7212 26.416C22.0645 26.0606 22.0645 25.4841 21.7212 25.1287L16.9432 20.1828L21.721 15.237C22.0644 14.8814 22.0644 14.305 21.721 13.9495C21.3777 13.594 20.8208 13.594 20.4773 13.9495L15.0775 19.5393C14.9058 19.7171 14.8201 19.9498 14.8201 20.1827C14.8201 20.4158 14.906 20.6487 15.0776 20.8264Z" fill="black" />
			</svg>
		</button>

		<button class="arrow-button arrow-button-special arrow-right" data-factor="1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" fill="#F8F8F8" />
				<path opacity="0.5" d="M24.9224 20.8264L19.5226 26.416C19.1791 26.7718 18.6221 26.7718 18.2788 26.416C17.9355 26.0606 17.9355 25.4841 18.2788 25.1287L23.0568 20.1828L18.279 15.237C17.9356 14.8814 17.9356 14.305 18.279 13.9495C18.6223 13.594 19.1792 13.594 19.5227 13.9495L24.9225 19.5393C25.0942 19.7171 25.1799 19.9498 25.1799 20.1827C25.1799 20.4158 25.094 20.6487 24.9224 20.8264Z" fill="black" />
			</svg>
		</button>

	</section>
	<!--special-collection-->

	<section class="bestsellers page-wrapper">
		<h2 class="bestsellers__title">Наши бестселлеры</h2>

		<div class="bestsellers-list-wrapper">
			<ul class="bestsellers-list">

				<?php
				$args = array(
					'tag' => array('bestseller'),
				);
				$products = wc_get_products($args);
				foreach ($products as $product) :
					$tags = get_the_terms($product->id, 'product_tag');
				?>
					<div class="swiper-item1 swiper-item1--margin">
						<div class="flower-picture">
							<a href="<?php echo $product->get_permalink(); ?>">
								<?php echo $product->get_image('woocommerce_thumbnail', ['class' => 'flower-img']); ?>
							</a>
							<?php if ($tags) : ?>
								<div class="adds adds--slider1">
									<?php
									foreach ($tags as $tag) :
										get_template_part('tags/' . $tag->slug);
									endforeach;
									?>
								</div>
							<?php endif; ?>
						</div>

						<div class="buy-block">
							<div class="wrapper-top">
								<div class="wrapper-left">
									<p class="flower__title"><?php echo $product->get_title(); ?></p>
									<span class="flower__price"><?php echo $product->get_price(); ?>,00 &#8381;</span>
								</div>
								<!-- <span class="buy-block__number">Выбрать кол - во <br> цветков</span>
								<div class="mount-card">
									<div class="mount-card-wrapper">
										<ul class="block-number">
											<li class="block-number__item">11 шт</li>
											<li class="block-number__item">17 шт</li>
											<li class="block-number__item">25 шт</li>
											<li class="block-number__item">37 шт</li>
										</ul>
										<span class="block-number__text">или введите свой вариант:</span>
										<div class="enter-wrapper">
											<input class="block-number__input" id="input-number" type="text" placeholder="Введите число">
											<button class="block-number__submit button" type="submit">Выбрать</button>

										</div>
									</div>
								</div>
								<span class="buy-block__number--short">Кол - во цветков</span> -->
								<!-- <div class="sizes-menu">

									<span class="sizes-menu__title">Размер букета:</span>
									<div class="radio-wrapper">
										<div class="sizes-radio sizes-radio--s">
											<div class="sizes-menu__div">S</div>
											<div class="popup-size">30 см</div>
										</div>

										<div class="sizes-radio sizes-radio--m">
											<div class="sizes-menu__div">M</div>
											<div class="popup-size">45 см</div>

										</div>

										<div class="sizes-radio sizes-radio--l">
											<div class="sizes-menu__div">L</div>
											<div class="popup-size">65 см</div>
										</div>
									</div>


								</div> -->

							</div>


							<!-- <div class="mount-card-mobile">
								<ul class="card-mobile__list">
									<li class="card-mobile__item">11 шт</li>
									<li class="card-mobile__item">17 шт</li>
									<li class="card-mobile__item">25 шт</li>
									<li class="card-mobile__item">37 шт</li>
								</ul>
							</div> -->

							<div class="wrapper-bottom">
								<button class="cart-button button">В корзину</button>
								<a href="#" class="buy-button">Купить в 1 клик</a>
							</div>
						</div>
					</div>
				<?php
				endforeach;
				?>

			</ul>
		</div>

		<button class="button1 button-position button">Показать еще</button>

		<button class="arrow-button arrow-button-bestsellers arrow-left" data-factor="-1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" transform="matrix(-1 0 0 1 40 0)" fill="#F8F8F8" />
				<path opacity="0.5" d="M15.0776 20.8264L20.4774 26.416C20.8209 26.7718 21.3779 26.7718 21.7212 26.416C22.0645 26.0606 22.0645 25.4841 21.7212 25.1287L16.9432 20.1828L21.721 15.237C22.0644 14.8814 22.0644 14.305 21.721 13.9495C21.3777 13.594 20.8208 13.594 20.4773 13.9495L15.0775 19.5393C14.9058 19.7171 14.8201 19.9498 14.8201 20.1827C14.8201 20.4158 14.906 20.6487 15.0776 20.8264Z" fill="black" />
			</svg>
		</button>

		<button class="arrow-button arrow-button-bestsellers arrow-right" data-factor="1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" fill="#F8F8F8" />
				<path opacity="0.5" d="M24.9224 20.8264L19.5226 26.416C19.1791 26.7718 18.6221 26.7718 18.2788 26.416C17.9355 26.0606 17.9355 25.4841 18.2788 25.1287L23.0568 20.1828L18.279 15.237C17.9356 14.8814 17.9356 14.305 18.279 13.9495C18.6223 13.594 19.1792 13.594 19.5227 13.9495L24.9225 19.5393C25.0942 19.7171 25.1799 19.9498 25.1799 20.1827C25.1799 20.4158 25.094 20.6487 24.9224 20.8264Z" fill="black" />
			</svg>
		</button>

	</section>
	<!--bestsellers page-wrapper-->

	<section class="new-items page-wrapper">
		<h2 class="new-items__title">Новинки</h2>
		<div class="slider2-block">
			<div class="slider2">
				<?php
				$args = array(
					'tag' => array('new'),
				);
				$products = wc_get_products($args);
				foreach ($products as $product) :
					$tags = get_the_terms($product->id, 'product_tag');
				?>
					<div class="swiper-item2">
						<div class="flower-card-items">
							<a href="<?php echo $product->get_permalink(); ?>">
								<?php echo $product->get_image('thumbnail', ['class' => 'swiper-img2']); ?>
							</a>
							<div class="adds adds--new-items">
								<?php
								foreach ($tags as $tag) :
									get_template_part('tags/' . $tag->slug);
								endforeach;
								?>
							</div>
						</div>
						<div class="buy-block2">
							<div class="wrapper-top wrapper-top-sizes">
								<div class="wrapper-left2">
									<p class="flower__title2"><?php echo $product->get_title(); ?></p>
									<span class="flower__price2"><?php echo $product->get_price(); ?>,00 &#8381;</span>
								</div>
								<div class="sizes-menu">
									<span class="sizes-menu__title mark-rotate">Размер:</span>
									<div class="radio-wrapper">
										<div class="sizes-radio sizes-radio--s">
											<div class="sizes-menu__div">S</div>
											<div class="popup-size">30 см</div>
										</div>

										<div class="sizes-radio sizes-radio--m">
											<div class="sizes-menu__div">M</div>
											<div class="popup-size">45 см</div>

										</div>

										<div class="sizes-radio sizes-radio--l">
											<div class="sizes-menu__div">L</div>
											<div class="popup-size">65 см</div>
										</div>
									</div>
								</div>

								<!-- блок выбора кол-ва цветков -->

								<!-- <span class="buy-block__number">Выбрать кол - во <br> цветков

								</span>
								<div class="mount-card mount-card-min">
									<div class="mount-card-wrapper">
										<ul class="block-number">
											<li class="block-number__item">11 шт</li>
											<li class="block-number__item">17 шт</li>
											<li class="block-number__item">25 шт</li>
											<li class="block-number__item">37 шт</li>
										</ul>
										<span class="block-number__text">или введите свой вариант:</span>
										<div class="enter-wrapper">
											<input class="block-number__input" id="input-number" type="text" placeholder="Введите число">
											<button class="block-number__submit" type="submit">Выбрать</button>
										
										</div>					
									</div>
								</div> -->
							</div>
							<div class="sizes-menu-tablet">
								<ul class="sizes-list-tablet">
									<li class="sizes-item-tablet">S &#40;30см&#41;</li>
									<li class="sizes-item-tablet">M &#40;50см&#41;</li>
									<li class="sizes-item-tablet">L &#40;60см&#41;</li>
								</ul>
							</div>
							<div class="wrapper-bottom2">
								<button class="cart-button cart-button2 button">В корзину</button>
								<a href="#" class="buy-button buy-button2">Купить в 1 клик</a>
							</div>
						</div>

					</div>
				<?php
				endforeach;
				?>

			</div>
		</div>

		<button class="arrow-button arrow-button-new arrow-left" data-factor="-1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" transform="matrix(-1 0 0 1 40 0)" fill="#F8F8F8" />
				<path opacity="0.5" d="M15.0776 20.8264L20.4774 26.416C20.8209 26.7718 21.3779 26.7718 21.7212 26.416C22.0645 26.0606 22.0645 25.4841 21.7212 25.1287L16.9432 20.1828L21.721 15.237C22.0644 14.8814 22.0644 14.305 21.721 13.9495C21.3777 13.594 20.8208 13.594 20.4773 13.9495L15.0775 19.5393C14.9058 19.7171 14.8201 19.9498 14.8201 20.1827C14.8201 20.4158 14.906 20.6487 15.0776 20.8264Z" fill="black" />
			</svg>
		</button>

		<button class="arrow-button arrow-button-new arrow-right" data-factor="1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" fill="#F8F8F8" />
				<path opacity="0.5" d="M24.9224 20.8264L19.5226 26.416C19.1791 26.7718 18.6221 26.7718 18.2788 26.416C17.9355 26.0606 17.9355 25.4841 18.2788 25.1287L23.0568 20.1828L18.279 15.237C17.9356 14.8814 17.9356 14.305 18.279 13.9495C18.6223 13.594 19.1792 13.594 19.5227 13.9495L24.9225 19.5393C25.0942 19.7171 25.1799 19.9498 25.1799 20.1827C25.1799 20.4158 25.094 20.6487 24.9224 20.8264Z" fill="black" />
			</svg>
		</button>

	</section>

	<section class="main-sections page-wrapper">
		<?php
		$product_categories = get_terms('product_cat');

		foreach ($product_categories as $i => $cat) :
		?>

			<div class="main-sections__item">
				<img class="main-sections__img main-sections__img--big--top" src="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/comp/main-sections-0<?php echo $i + 1; ?>.jpg">
				<a href="#" class="main-sections__title main-sections__title--big"><?php echo $cat->name; ?></a>
			</div>

		<?php
		endforeach;
		?>


	</section>

	<article class="wish-card">
		<div class="wish-card__text">
			<h2 class="wish-card__title">Мы подпишем <br> от вас карточку пожеланий</h2>
			<p class="wish-card__content">К букету мы всегда предлагаем приложить нашу фирменную карточку с текстом ваших пожеланий, а также приложим средство, продлевающее жизнь цветов</p>

		</div>
		<div class="wish-card-card">
			<img class="wish-card__img" src="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/wish-card.png" alt="City Flowers Wish Card">
		</div>
	</article>

	<?php /*<section class="seen-products page-wrapper">
		<h2 class="seen-products__title">Вы уже смотрели</h2>
		<div class="slider3-block">
			<div class="slider3">

				<?php for ($i = 0; $i < 10; $i++) {
				?>
					<div class="swiper-item3">
						<div class="flower-card-seen">
								<a href="#">
									<img class="swiper-img3" src="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/comp/seen0<?php echo $i + 1 ?>.jpg">
	
								</a>
	
	
							<div class="adds adds--seen-products">
								<div class="adds__main zip">
									<svg width="9" height="15" viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path fill-rule="evenodd" clip-rule="evenodd" d="M5.41572 0.435353C6.27539 -0.568769 7.85434 0.307521 7.52796 1.60757L6.46445 5.83754L8.22506 6.56874C9.01762 6.89791 9.25532 7.94294 8.68877 8.60511L3.58428 14.5646C2.72461 15.5688 1.14566 14.6925 1.47204 13.3924L2.53555 9.16246L0.774937 8.43126C-0.0176167 8.10273 -0.255321 7.05706 0.311228 6.39489L5.41572 0.435353ZM6.46599 1L1 6.75319L3.9658 7.86322L2.53401 13L8 7.24681L5.0342 6.13678L6.46599 1Z" fill="white" />
									</svg>
								</div>
								<span class="adds__main percents">15%</span>
	
							</div>
						</div>

						<div class="buy-block3">
							<div class="wrapper-top3">
								<div class="wrapper-left3">
									<p class="flower__title3">Монобукет гортензия</p>
									<span class="flower__price3">1 990,00 &#8381;</span>
								</div>
								<div class="wrapper-bottom3">
									<button class="buy-block3__button button">Собрать похожий</button>

								</div>
							</div>

						</div>

					</div>

				<?php
				} ?>

			</div>
		</div>

		<button class="arrow-button arrow-button-seen arrow-left" data-factor="-1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" transform="matrix(-1 0 0 1 40 0)" fill="#F8F8F8" />
				<path opacity="0.5" d="M15.0776 20.8264L20.4774 26.416C20.8209 26.7718 21.3779 26.7718 21.7212 26.416C22.0645 26.0606 22.0645 25.4841 21.7212 25.1287L16.9432 20.1828L21.721 15.237C22.0644 14.8814 22.0644 14.305 21.721 13.9495C21.3777 13.594 20.8208 13.594 20.4773 13.9495L15.0775 19.5393C14.9058 19.7171 14.8201 19.9498 14.8201 20.1827C14.8201 20.4158 14.906 20.6487 15.0776 20.8264Z" fill="black" />
			</svg>
		</button>

		<button class="arrow-button arrow-button-seen arrow-right" data-factor="1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" fill="#F8F8F8" />
				<path opacity="0.5" d="M24.9224 20.8264L19.5226 26.416C19.1791 26.7718 18.6221 26.7718 18.2788 26.416C17.9355 26.0606 17.9355 25.4841 18.2788 25.1287L23.0568 20.1828L18.279 15.237C17.9356 14.8814 17.9356 14.305 18.279 13.9495C18.6223 13.594 19.1792 13.594 19.5227 13.9495L24.9225 19.5393C25.0942 19.7171 25.1799 19.9498 25.1799 20.1827C25.1799 20.4158 25.094 20.6487 24.9224 20.8264Z" fill="black" />
			</svg>
		</button>

	</section>
	*/ ?>

	<section class="city-inst page-wrapper">
		<div class="wrapper-link">
			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<g clip-path="url(#clip0)">
					<path d="M12.0039 5.83813C8.60095 5.83813 5.84595 8.59614 5.84595 11.9961C5.84595 15.3991 8.60395 18.1541 12.0039 18.1541C15.4069 18.1541 18.1619 15.3961 18.1619 11.9961C18.1619 8.59314 15.4039 5.83813 12.0039 5.83813ZM12.0039 15.9931C9.79495 15.9931 8.00695 14.2041 8.00695 11.9961C8.00695 9.78814 9.79595 7.99914 12.0039 7.99914C14.2119 7.99914 16.0009 9.78814 16.0009 11.9961C16.0019 14.2041 14.2129 15.9931 12.0039 15.9931Z" fill="#F5257C" />
					<path d="M16.948 0.0758749C14.74 -0.0271251 9.27098 -0.0221251 7.06098 0.0758749C5.11898 0.166875 3.40598 0.635875 2.02498 2.01687C-0.283017 4.32487 0.0119831 7.43488 0.0119831 11.9959C0.0119831 16.6639 -0.248017 19.7019 2.02498 21.9749C4.34198 24.2909 7.49698 23.9879 12.004 23.9879C16.628 23.9879 18.224 23.9909 19.859 23.3579C22.082 22.4949 23.76 20.5079 23.924 16.9389C24.028 14.7299 24.022 9.26188 23.924 7.05188C23.726 2.83888 21.465 0.283875 16.948 0.0758749ZM20.443 20.4479C18.93 21.9609 16.831 21.8259 11.975 21.8259C6.97498 21.8259 4.96998 21.8999 3.50698 20.4329C1.82198 18.7559 2.12698 16.0629 2.12698 11.9799C2.12698 6.45487 1.55998 2.47587 7.10498 2.19187C8.37898 2.14687 8.75398 2.13187 11.961 2.13187L12.006 2.16187C17.335 2.16187 21.516 1.60387 21.767 7.14787C21.824 8.41288 21.837 8.79288 21.837 11.9949C21.836 16.9369 21.93 18.9539 20.443 20.4479Z" fill="#F5257C" />
					<path d="M18.406 7.034C19.2008 7.034 19.845 6.38974 19.845 5.595C19.845 4.80027 19.2008 4.15601 18.406 4.15601C17.6113 4.15601 16.967 4.80027 16.967 5.595C16.967 6.38974 17.6113 7.034 18.406 7.034Z" fill="#F5257C" />
				</g>
				<defs>
					<clipPath id="clip0">
						<rect width="24" height="24" fill="white" />
					</clipPath>
				</defs>
			</svg>
			<a class="city-inst__link" href="#">cityflowers_ru</a>
		</div>

		<div class="inst-photos-wrapper">
			<ul class="city-inst__photos">
				<?php for ($i = 0; $i < 5; $i++) {
				?>
					<li class="city-inst__item">
						<img class="city-inst__img" src="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/comp/city-inst0<?php echo $i + 1 ?>.jpg">
					</li>

				<?php
				} ?>


			</ul>
		</div>


		<button class="arrow-button arrow-button-inst arrow-left" data-factor="-1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" transform="matrix(-1 0 0 1 40 0)" fill="#F8F8F8" />
				<path opacity="0.5" d="M15.0776 20.8264L20.4774 26.416C20.8209 26.7718 21.3779 26.7718 21.7212 26.416C22.0645 26.0606 22.0645 25.4841 21.7212 25.1287L16.9432 20.1828L21.721 15.237C22.0644 14.8814 22.0644 14.305 21.721 13.9495C21.3777 13.594 20.8208 13.594 20.4773 13.9495L15.0775 19.5393C14.9058 19.7171 14.8201 19.9498 14.8201 20.1827C14.8201 20.4158 14.906 20.6487 15.0776 20.8264Z" fill="black" />
			</svg>
		</button>

		<button class="arrow-button arrow-button-inst arrow-right" data-factor="1">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="40" height="40" rx="20" fill="#F8F8F8" />
				<path opacity="0.5" d="M24.9224 20.8264L19.5226 26.416C19.1791 26.7718 18.6221 26.7718 18.2788 26.416C17.9355 26.0606 17.9355 25.4841 18.2788 25.1287L23.0568 20.1828L18.279 15.237C17.9356 14.8814 17.9356 14.305 18.279 13.9495C18.6223 13.594 19.1792 13.594 19.5227 13.9495L24.9225 19.5393C25.0942 19.7171 25.1799 19.9498 25.1799 20.1827C25.1799 20.4158 25.094 20.6487 24.9224 20.8264Z" fill="black" />
			</svg>
		</button>

	</section>

	<div class="one-click">
		<div class="one-click__wrapper">

			<button type="button" class="one-click__button">Закрыть<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path width="27" height="27" opacity="0.5" fill-rule="evenodd" clip-rule="evenodd" d="M9.99972 7.99338L17.5775 0.415598C18.1316 -0.138533 19.0301 -0.138533 19.5842 0.415598C20.1383 0.969729 20.1383 1.86815 19.5842 2.42228L12.0064 10.0001L19.584 17.5777C20.1382 18.1318 20.1382 19.0303 19.584 19.5844C19.0299 20.1385 18.1315 20.1385 17.5774 19.5844L9.99972 12.0068L2.42243 19.584C1.8683 20.1382 0.969877 20.1382 0.415746 19.584C-0.138385 19.0299 -0.138385 18.1315 0.415746 17.5774L7.99303 10.0001L0.415598 2.42264C-0.138533 1.86851 -0.138533 0.970081 0.415598 0.41595C0.969729 -0.138181 1.86815 -0.138181 2.42228 0.415951L9.99972 7.99338Z" fill="black" />
				</svg></button>
			<h2 class="one-click__title">Купить в 1 клик</h2>

			<!-- <form class="one-click__form" method="POST" action="#">
				<div class="user user-wrapper">
					<p class="user__cell">
						<label class="user__label" for="user-name" name="your-name">Ваше имя</label>
						<input class="user__input" id="user-name" type="text" placeholder="Введите данные">
					</p>
					<p class="user__cell">
						<label class="user__label" for="recipient" name="recipient-name">Имя получателя</label>
						<input class="user__input" id="recipient" type="text" placeholder="Введите данные">
					</p>
					<p class="user__cell">
						<label class="user__label" for="user-tel" name="telephone">Телефон</label>
						<input class="user__input" id="user-tel" type="text" placeholder="+7 (___) ___-__-__">
					</p>
					<p class="user__cell">
						<label class="user__label" for="user-adress" name="your-adress">Адрес доставки</label>
						<input class="user__input" id="user-adress" type="text" placeholder="Введите данные">
					</p>
					<p class="user__cell">
						<label class="user__label" for="user-mail" name="email">Email</label>
						<input class="user__input" id="user-mail" type="text" placeholder="mail@info.ru">
					</p>
					<p class="user__cell">
						<label class="user__label" for="user-coment" name="coments">Ваши коментарии</label>
						<input class="user__input" id="user-coment" type="text" placeholder="Введите данные">
					</p>
				</div>
				<label class="agreement" for="user-agree">
					<input id="user-agree" type="checkbox" name="agree" class="visually-hidden">
					<span class="checkbox-indicator"></span>
					Я согласен на <a class="agreement__process-link" href="#">обработку персональных данных</a>

				</label>
				<button class="user__button-submit cart-button button" type="submit">Отправить</button>

			</form> -->

			<?php 
			echo do_shortcode('[wpforms id="150"]');

			?>
		</div>

	</div>


</main>
