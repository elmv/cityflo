<?php
global $product;

$tags = get_the_terms($product->id, 'product_tag');
$img = wp_get_attachment_image_src( $product->get_image_id(), 'large' )[0];
?>
<div class="swiper-item1 swiper-item1--margin">
	<div class="flower-picture">
		<a href="<?php echo $product->get_permalink(); ?>">
			<div style="background-image: url(<?php echo $img ?>);" class="flower-img"></div>
		</a>
		<?php if ($tags) : ?>
			<div class="adds adds--slider1">
				<?php
				foreach ($tags as $tag) :
					get_template_part('tags/' . $tag->slug);
				endforeach;
				?>
			</div>
		<?php endif; ?>
	</div>

	<div class="buy-block">
		<div class="wrapper-top">
			<div class="wrapper-left">
				<p class="flower__title"><?php echo $product->get_title(); ?></p>
				<span class="flower__price flower__pricej"><?php echo good_product_price(); ?>,00 &#8381;</span>
			</div>
			<?php if (!$product->is_type('variable')) { ?>
				<span class="buy-block__number">Выбрать кол - во <br> цветков</span>
				<div class="mount-card">
					<div class="mount-card-wrapper">
						<ul class="block-number">
							<li data-count="11" class="block-number__item">11 шт</li>
							<li data-count="17" class="block-number__item">17 шт</li>
							<li data-count="25" class="block-number__item">25 шт</li>
							<li data-count="37" class="block-number__item">37 шт</li>
						</ul>
						<span class="block-number__text">или введите свой вариант:</span>
						<div class="enter-wrapper">
							<input class="block-number__input" id="input-number" type="text" placeholder="Введите число">
							<button class="block-number__submit button" type="submit">Выбрать</button>
						</div>
					</div>
				</div>
				<!-- <span class="buy-block__number--short">Кол - во цветков</span> -->
			<?php } ?>
			<?php if ($product->is_type('variable')) { ?>
				<div class="sizes-menu">
					<span class="sizes-menu__title">Размер:</span>
					<div class="radio-wrapper pa-size-block">
						<div class="sizes-radio sizes-radio--s">
							<div class="sizes-menu__div">S</div>
							<div class="popup-size">30 см</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>

		<?php if ($product->is_type('variable')) { ?>
			<div class="mount-card-mobile">
				<ul class="card-mobile__list">
					<li class="card-mobile__item">11 шт</li>
					<li class="card-mobile__item">17 шт</li>
					<li class="card-mobile__item">25 шт</li>
					<li class="card-mobile__item">37 шт</li>
				</ul>
			</div>
		<?php } ?>
		<div class="wrapper-bottom">
			<?php iconic_template_loop_add_to_cart(); ?>
			<a href="#" class="buy-button">Купить в 1 клик</a>
		</div>
	</div>
</div>