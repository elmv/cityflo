<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package cityflo
 */

?>

</div><!-- .col-full -->
</div><!-- #content -->

<footer class="page-footer">

	<div class="page-footer__wrapper page-wrapper">
		<div class="page-footer__section1-wrapper">
			<a href="/" class="page-footer__logo">
				<img src="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/footer-logo.svg" alt="логотип City Flowers">
			</a>

			<ul class="page-footer__list">
				<li class="page-footer__item-section2">
					<a class="page-footer__link-section2 footer-bouquet-item" href="#">Букеты</a>
					<div class="page-header__sub-menu-list-wrapper">
						<ul class="page-header__sub-menu-list">
							<li class="page-header__sub-menu-item">
								<a class="page-header__sub-menu-link" href="/product-category/authors_bouquets/">Авторские букеты</a>
								<ul class="page-header__sub-sub-menu-list">
									<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="#">Авторские букеты до 2000 р.</a></li>
									<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="#">Авторские букеты до 5000 р.</a></li>
									<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="#">Авторские букеты до 15000 р.</a></li>

								</ul>
							</li>
							<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/monobouquets/">Монобукеты</a></li>
							<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/duotriobouquets/">Дуо и трио букеты</a></li>
							<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/compositions/">Композиции</a></li>
							<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/bridesbouquet/">Букет невесты</a></li>

						</ul>
					</div>					
				</li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/product-category/companies/">Компаниям</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="#">Оформление</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/product-category/bridesbouquet/">Букет невесты</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/contacts/">Контакты</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/shipping-payment/">Доставка и оплата</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/about/">О нас</a></li>
				
			</ul>

			<div class="page-footer__contacts-wrapper">

				<p class="page-footer__contacts page-contacts">
					<a class="page-footer__tel page-tel" href="tel:+79164254665">+7 (916) 425-46-65</a>
					<span class="page-footer__text page-text">Phone/ WhatsApp/ Telegram</span>
				</p>
			</div>

		</div>



		<div class="page-footer__section2-wrapper">
			<span class="page-footer__rights">&copy;Все права защищены</span>

			<ul class="page-footer__docs-list">
				<li class="page-footer__docs-item">
					<a href="/return-conditions/">
						Условия возврата
					</a>
				</li>
				<li class="page-footer__docs-item">
					<a href="/privacy-policy/">
						Политика обработки персональных данных
					</a>
				</li>
				<li class="page-footer__docs-item">
					<a href="/public-offer/">
						Публичная оферта
					</a>
				</li>
			</ul>

		</div>

	</div>
	<!--page-footer__wrapper page-wrapper-->

</footer>

</div>

<script type="text/javascript" src="/wp-content/themes/cityflo/scripts/bundleb946bd0e3399197f0c22.js"></script>

<?php wp_footer(); ?>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.7.2/js/lightgallery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>


</body>

</html>
