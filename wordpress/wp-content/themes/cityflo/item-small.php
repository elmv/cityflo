<?php
global $product;

$tags = get_the_terms($product->id, 'product_tag');
$img = wp_get_attachment_image_src( $product->get_image_id(), 'large' )[0];
?>
<div class="swiper-item2">
	<div class="flower-card-items">
		<a href="<?php echo $product->get_permalink(); ?>">
			<div style="background-image: url(<?php echo $img ?>);" class="swiper-img2"></div>
		</a>
		<div class="adds adds--new-items">
			<?php
			if($tags) {
				foreach ($tags as $tag) :
					get_template_part('tags/' . $tag->slug);
				endforeach;
			}
			?>
		</div>
	</div>
	<div class="buy-block2">
		<div class="wrapper-top wrapper-top-sizes">
			<div class="wrapper-left2">
				<p class="flower__title2"><?php echo $product->get_title(); ?></p>
				<span class="flower__price2 flower__pricej"><?php echo good_product_price(); ?>,00 &#8381;</span>
			</div>
			<?php if ($product->is_type('variable')) { ?>
				<div class="sizes-menu">
					<span class="sizes-menu__title mark-rotate">Размер:</span>
					<div class="radio-wrapper pa-size-block">
						<div class="sizes-radio sizes-radio--s">
							<div class="sizes-menu__div">S</div>
							<div class="popup-size">30 см</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if (!$product->is_type('variable')) { ?>
				<!-- блок выбора кол-ва цветков -->
				<span class="buy-block__number">Выбрать кол - во <br> цветков
				</span>
				<div class="mount-card mount-card-min">
					<div class="mount-card-wrapper">
						<ul class="block-number">
							<li data-count="11" class="block-number__item">11 шт</li>
							<li data-count="17" class="block-number__item">17 шт</li>
							<li data-count="25" class="block-number__item">25 шт</li>
							<li data-count="37" class="block-number__item">37 шт</li>
						</ul>
						<span class="block-number__text">или введите свой вариант:</span>
						<div class="enter-wrapper">
							<input class="block-number__input" id="input-number" type="text" placeholder="Введите число">
							<button class="block-number__submit" type="submit">Выбрать</button>

						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<?php if ($product->is_type('variable')) { ?>
			<div class="sizes-menu-tablet">
				<ul class="sizes-list-tablet">
					<li class="sizes-item-tablet">S &#40;30см&#41;</li>
					<li class="sizes-item-tablet">M &#40;50см&#41;</li>
					<li class="sizes-item-tablet">L &#40;60см&#41;</li>
				</ul>
			</div>
		<?php } ?>
		<div class="wrapper-bottom2">
			<?php iconic_template_loop_add_to_cart(); ?>
			<a href="#" class="buy-button buy-button2">Купить в 1 клик</a>
		</div>
	</div>

</div>