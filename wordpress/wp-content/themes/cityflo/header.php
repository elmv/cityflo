<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package cityflo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="shortcut icon" href="/wp-content/themes/cityflo/assets/images/loaded/favicon-32x32.png" type="image/png">

	<?php wp_head(); ?>
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.7.2/css/lightgallery.min.css"> -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">

	<link href="/wp-content/themes/cityflo/style/styleb946bd0e3399197f0c22.css" rel="stylesheet">

	<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

</head>

<body <?php body_class(); ?>>

	<?php wp_body_open(); ?>

	<div id="page" class="hfeed site">

		<header class="page-header page-wrapper">
			<div class="page-header__section-top-wrapper">

				<a href="#" class="page-header__burgermenu">
					<span></span>
					<span></span>
					<span></span>
				</a>
				<a href="/" class="page-header__logo">

					<img src="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/logo.svg" alt="логотип City Flowers">

				</a>
				<div class="page-header__contacts page-contacts">
					<a class="page-header__tel page-tel" href="tel:+79164254665">+7 (916) 425-46-65</a>
					<span class="page-header__text page-text">Phone/ WhatsApp/ Telegram</span>
				</div>

				<div class="page-header__time">
					<p class="page-header__works">Режим работы салона:</p>
					<span class="page-header__text page-text">09:00-19:00 пн-пт</span>
				</div>

				<div class="page-header__delivery">
					<p class="page-header__works">Принимаем и доставляем</p>
					<span class="page-header__text page-text">24/7 без выходных</span>
				</div>

				<div class="page-header__icons">
					<a href="/my-account/" class="page-header__login">
						<svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" clip-rule="evenodd" d="M9.96212 7.79761C11.4272 6.64993 12.003 4.69922 11.396 2.9399C10.7889 1.18057 9.13273 0 7.27162 0C5.41051 0 3.7543 1.18057 3.14726 2.9399C2.54022 4.69922 3.11601 6.64993 4.58112 7.79761C1.81759 8.89822 0.00306972 11.571 0 14.5457V15.2728C0 15.6744 0.325562 16 0.727162 16C1.12876 16 1.45432 15.6744 1.45432 15.2728V14.5457C1.45432 11.3329 4.05882 8.72838 7.27162 8.72838C10.4844 8.72838 13.0889 11.3329 13.0889 14.5457V15.2728C13.0889 15.6744 13.4145 16 13.8161 16C14.2177 16 14.5432 15.6744 14.5432 15.2728V14.5457C14.5402 11.571 12.7257 8.89822 9.96212 7.79761ZM4.36276 4.36542C4.36276 2.75902 5.66501 1.45677 7.27141 1.45677C8.87781 1.45677 10.1801 2.75902 10.1801 4.36542C10.1801 5.97182 8.87781 7.27407 7.27141 7.27407C6.49999 7.27407 5.76016 6.96762 5.21468 6.42214C4.66921 5.87667 4.36276 5.13684 4.36276 4.36542Z" fill="#0E1317" />
						</svg>
					</a>
					<?php storefront_cart_link(); ?>
				</div>

			</div>

			<div class="page-header__section-bottom-wrapper">
				<ul class="page-header__menu">
					<!-- <li class="page-header__item-section2">
						<a class="page-header__link-section2" href="/product-category/september-1/">1 сентября</a>
					</li> -->
					<li class="page-header__item-section2">

						<a class="page-header__link-section2 phls__sub" href="#">Букеты</a>
						<div class="page-header__sub-menu-list-wrapper">
							<ul class="page-header__sub-menu-list">
								<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/march-8/">8 марта</a></li>
								<li class="page-header__sub-menu-item">
									<a class="page-header__sub-menu-link" href="/product-category/authors_bouquets/">Авторские букеты</a>
									<ul class="page-header__sub-sub-menu-list">
										<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="/product-category/authors_bouquets/?max_price=2000">Авторские букеты до 2000 р.</a></li>
										<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="/product-category/authors_bouquets/?max_price=5000">Авторские букеты до 5000 р.</a></li>
										<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="/product-category/authors_bouquets/?max_price=15000">Авторские букеты до 15000 р.</a></li>

									</ul>
								</li>
								<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/monobouquets/">Монобукеты</a></li>
								<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/duotriobouquets/">Дуо и трио букеты</a></li>
								<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/compositions/">Композиции</a></li>
								<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/bridesbouquet/">Букет невесты</a></li>

							</ul>
						</div>

					</li>

					<li class="page-header__item-section2">

						<a class="page-header__link-section2" href="/companies/">Компаниям</a>
						<div class="page-header__sub-menu-list-wrapper">

							<ul class="page-header__sub-menu-list">
								<li><a class="page-header__sub-menu-link" href="#"></a></li>
							</ul>
						</div>

					</li>

					<li class="page-header__item-section2">

						<a class="page-header__link-section2" href="/decor/">Оформление</a>
						<div class="page-header__sub-menu-list-wrapper">

							<ul class="page-header__sub-menu-list">
								<li><a class="page-header__sub-menu-link" href="#"></a></li>
							</ul>
						</div>

					</li>

					<li class="page-header__item-section2">
						<a class="page-header__link-section2" href="/product-category/bridesbouquet/">Букет невесты</a>
						<div class="page-header__sub-menu-list-wrapper">

							<ul class="page-header__sub-menu-list">
								<li><a class="page-header__sub-menu-link" href="#"></a></li>
							</ul>
						</div>

					</li>
					<li class="page-header__item-section2">
						<a class="page-header__link-section2" href="/contacts/">Контакты</a>
						<div class="page-header__sub-menu-list-wrapper">

							<ul class="page-header__sub-menu-list">
								<li><a class="page-header__sub-menu-link" href="#"></a></li>
							</ul>
						</div>
					</li>
					<li class="page-header__item-section2">
						<a class="page-header__link-section2" href="/shipping-payment/">Доставка и оплата</a>
					</li>
					<li class="page-header__item-section2">
						<a class="page-header__link-section2" href="/about/">О нас</a>
					</li>
				</ul>
				<a href="/my-account/" class="mobile-menu-wrapper-top">
					<h2>Войти в личный кабинет</h2>
					<div class="mobile-login">
						<svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" clip-rule="evenodd" d="M9.96212 7.79761C11.4272 6.64993 12.003 4.69922 11.396 2.9399C10.7889 1.18057 9.13273 0 7.27162 0C5.41051 0 3.7543 1.18057 3.14726 2.9399C2.54022 4.69922 3.11601 6.64993 4.58112 7.79761C1.81759 8.89822 0.00306972 11.571 0 14.5457V15.2728C0 15.6744 0.325562 16 0.727162 16C1.12876 16 1.45432 15.6744 1.45432 15.2728V14.5457C1.45432 11.3329 4.05882 8.72838 7.27162 8.72838C10.4844 8.72838 13.0889 11.3329 13.0889 14.5457V15.2728C13.0889 15.6744 13.4145 16 13.8161 16C14.2177 16 14.5432 15.6744 14.5432 15.2728V14.5457C14.5402 11.571 12.7257 8.89822 9.96212 7.79761ZM4.36276 4.36542C4.36276 2.75902 5.66501 1.45677 7.27141 1.45677C8.87781 1.45677 10.1801 2.75902 10.1801 4.36542C10.1801 5.97182 8.87781 7.27407 7.27141 7.git 27407C6.49999 7.27407 5.76016 6.96762 5.21468 6.42214C4.66921 5.87667 4.36276 5.13684 4.36276 4.36542Z" fill="#0E1317"></path>
						</svg>
					</div>
				</a>
				<div class="mobile-menu-wrapper-bottom">

					<div class="page-header__contacts mobile-contacts page-contacts">
						<a class="page-header__tel page-tel" href="tel:+79164254665">+7 (916) 425-46-65</a>
						<span class="page-header__text page-text">Phone/ WhatsApp/ Telegram</span>
					</div>

					<div class="page-header__time mobile-time">
						<p class="page-header__works">Режим работы салона:</p>
						<span class="page-header__text page-text">09:00-19:00 пн-пт</span>
					</div>

					<div class="page-header__delivery mobile-delivery">
						<p class="page-header__works">Принимаем и доставляем</p>
						<span class="page-header__text page-text">24/7 без выходных</span>
					</div>
				</div>

			</div>


		</header><!-- #masthead -->

		<div id="content" class="site-content">
			<div class="col-full">