<?php

/**
	 * cityflo engine room
	 *
	 * @package cityflo
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme('storefront');
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if (!isset($content_width)) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version'    => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';
require 'inc/wordpress-shims.php';

if (class_exists('Jetpack')) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if (storefront_is_woocommerce_activated()) {
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if (is_admin()) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */

// add_filter('wp_default_scripts', 'remove_jquery_migrate');

function remove_jquery_migrate(&$scripts)
{
	if (!is_admin()) {
		$scripts->remove('jquery');
	}
}

//Remove Gutenberg Block Library CSS from loading on the frontend
function smartwp_remove_wp_block_library_css()
{
	wp_dequeue_style('wp-block-library');
	wp_dequeue_style('wp-block-library-theme');
	wp_dequeue_style('wc-block-style'); // Remove WooCommerce block CSS
}
add_action('wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100);
add_filter('woocommerce_enqueue_styles', '__return_empty_array');


function instablock_func()
{
	ob_start();
	get_template_part('insta-block');
	return ob_get_clean();
}
add_shortcode('instablock', 'instablock_func');

function map_block_func()
{
	ob_start();
	get_template_part('delivery-map');
	return ob_get_clean();
}
add_shortcode('mapblock', 'map_block_func');

/**
 * Replace add to cart button in the loop.
 */
function iconic_change_loop_add_to_cart()
{
	remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
	add_action('woocommerce_after_shop_loop_item', 'iconic_template_loop_add_to_cart', 10);
}

add_action('init', 'iconic_change_loop_add_to_cart', 10);

/**
 * Use single add to cart button for variable products.
 */
function iconic_template_loop_add_to_cart()
{
	global $product;

	if (!$product->is_type('variable')) {
		woocommerce_template_loop_add_to_cart();
		return;
	}

	remove_action('woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20);
	add_action('woocommerce_single_variation', 'iconic_loop_variation_add_to_cart_button', 20);

	woocommerce_template_single_add_to_cart();
}

/**
 * Customise variable add to cart button for loop.
 *
 * Remove qty selector and simplify.
 */
function iconic_loop_variation_add_to_cart_button()
{
	global $product;

?>
	<div class="woocommerce-variation-add-to-cart variations_button">
		<button type="submit" class="single_add_to_cart_button cart-button custom_add_to_cart button"><?php echo esc_html($product->single_add_to_cart_text()); ?></button>
		<input type="hidden" name="add-to-cart" value="<?php echo absint($product->get_id()); ?>" />
		<input type="hidden" name="product_id" value="<?php echo absint($product->get_id()); ?>" />
		<input type="hidden" name="variation_id" class="variation_id" value="0" />
	</div>
<?php
}

function good_product_price()
{
	global $product;
	if ($product->is_type('simple')) {
		$price = $product->get_price();
	} elseif ($product->is_type('variable')) {
		foreach ($product->get_available_variations() as $pav) {
			$def = true;
			foreach ($product->get_variation_default_attributes() as $defkey => $defval) {
				if ($pav['attributes']['attribute_' . $defkey] != $defval) {
					$def = false;
				}
			}
			if ($def) {
				$price = $pav['display_price'];
			}
		}
	}

	return $price;
}

/*
 * Убираем поля для конкретного способа доставки
 */

add_filter('woocommerce_checkout_fields', 'awoohc_override_checkout_fields');

function awoohc_override_checkout_fields($fields)
{
	// получаем выбранные метод доставки
	$chosen_methods = WC()->session->get('chosen_shipping_methods');
	// проверяем текущий метод и убираем не ненужные поля
	$fields['billing']['billing_company']['required'] = false;
	unset($fields['billing']['billing_company']);

	$fields['billing']['billing_city']['required'] = false;
	unset($fields['billing']['billing_city']);

	$fields['billing']['billing_postcode']['required'] = false;
	unset($fields['billing']['billing_postcode']);

	unset($fields['billing']['billing_country']);
	$fields['billing']['billing_country']['required'] = false;

	$fields['billing']['billing_state']['required'] = false;
	unset($fields['billing']['billing_state']);

	$fields['billing']['billing_last_name']['required'] = false;
	unset($fields['billing']['billing_last_name']);
	$fields['billing']['billing_address_1']['required'] = false;
	$fields['billing']['billing_address_2']['required'] = false;


	// if ($chosen_methods[0] === 'local_pickup:2') {
	// 	//   unset( $fields['billing']['billing_email'] );
	// 	//   unset( $fields['billing']['billing_phone'] );
	// 	unset($fields['billing']['billing_address_1']);
	// 	unset($fields['billing']['billing_address_2']);
	// }

	return $fields;
}

function wpse_enqueue_datepicker()
{
	// Load the datepicker script (pre-registered in WordPress).
	wp_enqueue_script('jquery-ui-datepicker');

	// You need styling for the datepicker. For simplicity I've linked to the jQuery UI CSS on a CDN.
	wp_register_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
	wp_enqueue_style('jquery-ui');
}
add_action('wp_enqueue_scripts', 'wpse_enqueue_datepicker');

add_action('woocommerce_checkout_order_processed', 'checkout_process', 20, 1);

function checkout_process($order_id)
{

	$order = wc_get_order($order_id);

	//отфильтровываем только заказы в статусе "ожидание"
	if (!$order->has_status('pending')) return;
	$wc_email = WC()->mailer()->get_emails()['WC_Email_New_Order'];
	// Изменяем тему
	$wc_email->settings['subject'] = ('{site_title} - New customer Pending order ({order_number}) - {order_date}');
	// Изменяем заголовок     
	$wc_email->settings['heading'] = ('New customer Pending Order');
	// Отправить уведомление «Новое письмо» (администратору)
	$wc_email->trigger($order_id);
	// Сообщение пользователю
	$email_heading = 'Спасибо за заказ';
	$args = array(
		'order'         => $order,
		'email_heading' => $email_heading,
		'sent_to_admin' => false,
		'plain_text'    => false,
	);
	$content_info = wc_get_template_html("emails/customer-processing-order.php", $args);
	$site_title = get_bloginfo('name');
	$customer_email = $order->get_billing_email();
	$email_subject = $site_title . ' - Новый заказ';
	//  wc_mail($customer_email, $email_subject, $content_info);
}

add_filter('woocommerce_get_image_size_thumbnail', function ($size) {
	return array(
		'width' => 1040,
		'height' => 1040,
		'crop' => 1,
	);
});


add_filter('loop_shop_post_in', function () {
	if (isset($_GET['max_price'])) {
		// $matched_products_query = apply_filters( 'woocommerce_price_filter_results', $wpdb->get_results( $wpdb->prepare("
        //     SELECT DISTINCT ID, post_parent, post_type FROM $wpdb->posts
        //     INNER JOIN $wpdb->postmeta ON ID = post_id
        //     WHERE post_type IN ( 'product', 'product_variation' ) AND post_status = 'publish' AND meta_key = %s AND meta_value BETWEEN %d AND %d
        // ", '_price', $min, $max ), OBJECT_K ), $min, $max );
		// if ( $matched_products_query ) {
        //     foreach ( $matched_products_query as $product ) {
        //         if ( $product->post_type == 'product' )
        //             $matched_products[] = $product->ID;
        //         if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
        //             $matched_products[] = $product->post_parent;
        //     }
        // }
	}
});