<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package cityflo
 */

?>

</div><!-- .col-full -->
</div><!-- #content -->

<footer class="page-footer">

	<div class="page-footer__wrapper page-wrapper">
		<div class="page-footer__section1-wrapper">
			<a href="/" class="page-footer__logo">
				<img src="<?php echo bloginfo("template_url"); ?>/assets/images/loaded/footer-logo.svg" alt="логотип City Flowers">
			</a>

			<ul class="page-footer__list">
				<li class="page-footer__item-section2">
					<a class="page-footer__link-section2 footer-bouquet-item" href="#">Букеты</a>
					<div class="page-header__sub-menu-list-wrapper">
						<ul class="page-header__sub-menu-list">
							<li class="page-header__sub-menu-item">
								<a class="page-header__sub-menu-link" href="/product-category/authors_bouquets/">Авторские букеты</a>
								<ul class="page-header__sub-sub-menu-list">
									<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="#">Авторские букеты до 2000 р.</a></li>
									<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="#">Авторские букеты до 5000 р.</a></li>
									<li class="page-header__sub-sub-menu-item"><a class="page-header__sub-sub-menu-link" href="#">Авторские букеты до 15000 р.</a></li>

								</ul>
							</li>
							<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/monobouquets/">Монобукеты</a></li>
							<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/duotriobouquets/">Дуо и трио букеты</a></li>
							<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/compositions/">Композиции</a></li>
							<li class="page-header__sub-menu-item"><a class="page-header__sub-menu-link" href="/product-category/bridesbouquet/">Букет невесты</a></li>

						</ul>
					</div>					
				</li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/product-category/companies/">Компаниям</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="#">Оформление</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/product-category/bridesbouquet/">Букет невесты</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/contacts/">Контакты</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/shipping-payment/">Доставка и оплата</a></li>
				<li class="page-footer__item-section2"><a class="page-footer__link-section2" href="/about/">О нас</a></li>
				
			</ul>

			<div class="page-footer__contacts-wrapper">
				<a href="https://www.instagram.com/cityflowers_ru/?hl=ru" class="page-footer__icon">
					<svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
						<g clip-path="url(#clip0)">
							<path d="M10.5035 5.1084C7.52586 5.1084 5.11523 7.52165 5.11523 10.4966C5.11523 13.4743 7.52848 15.8849 10.5035 15.8849C13.4811 15.8849 15.8917 13.4716 15.8917 10.4966C15.8917 7.51902 13.4785 5.1084 10.5035 5.1084ZM10.5035 13.994C8.57061 13.994 7.00611 12.4286 7.00611 10.4966C7.00611 8.56465 8.57148 6.99927 10.5035 6.99927C12.4355 6.99927 14.0009 8.56465 14.0009 10.4966C14.0017 12.4286 12.4364 13.994 10.5035 13.994Z" fill="black" />
							<path d="M14.8295 0.0664516C12.8975 -0.0236734 8.11208 -0.0192984 6.17833 0.0664516C4.47908 0.146077 2.9802 0.556451 1.77183 1.76483C-0.24767 3.78433 0.0104547 6.50558 0.0104547 10.4965C0.0104547 14.581 -0.217045 17.2392 1.77183 19.2281C3.7992 21.2546 6.55983 20.9895 10.5035 20.9895C14.5495 20.9895 15.946 20.9921 17.3766 20.4382C19.3217 19.6831 20.79 17.9445 20.9335 14.8216C21.0245 12.8887 21.0192 8.1042 20.9335 6.17045C20.7602 2.48408 18.7818 0.248452 14.8295 0.0664516ZM17.8876 17.892C16.5637 19.2158 14.7271 19.0977 10.4781 19.0977C6.10308 19.0977 4.3487 19.1625 3.06858 17.8788C1.5942 16.4115 1.86108 14.0551 1.86108 10.4825C1.86108 5.64808 1.36495 2.16645 6.21683 1.91795C7.33158 1.87858 7.6597 1.86545 10.4658 1.86545L10.5052 1.8917C15.1681 1.8917 18.8265 1.40345 19.0461 6.25445C19.096 7.36133 19.1073 7.69383 19.1073 10.4956C19.1065 14.8198 19.1887 16.5847 17.8876 17.892Z" fill="black" />
							<path d="M16.1053 6.15472C16.8007 6.15472 17.3644 5.59099 17.3644 4.8956C17.3644 4.2002 16.8007 3.63647 16.1053 3.63647C15.4099 3.63647 14.8462 4.2002 14.8462 4.8956C14.8462 5.59099 15.4099 6.15472 16.1053 6.15472Z" fill="black" />
						</g>
						<defs>
							<clipPath id="clip0">
								<rect width="21" height="21" fill="white" />
							</clipPath>
						</defs>
					</svg>
				</a>

				<p class="page-footer__contacts page-contacts">
					<a class="page-footer__tel page-tel" href="tel:+79164254665">+7 (916) 425-46-65</a>
					<span class="page-footer__text page-text">Phone/ WhatsApp/ Telegram</span>
				</p>
			</div>

		</div>



		<div class="page-footer__section2-wrapper">
			<span class="page-footer__rights">&copy;Все права защищены</span>

			<ul class="page-footer__docs-list">
				<li class="page-footer__docs-item">
					<a href="/return-conditions/">
						Условия возврата
					</a>
				</li>
				<li class="page-footer__docs-item">
					<a href="/privacy-policy/">
						Политика обработки персональных данных
					</a>
				</li>
				<li class="page-footer__docs-item">
					<a href="/public-offer/">
						Публичная оферта
					</a>
				</li>
			</ul>

		</div>

	</div>
	<!--page-footer__wrapper page-wrapper-->

</footer>

</div>

<script type="text/javascript" src="/wp-content/themes/cityflo/scripts/bundle<%= htmlWebpackPlugin.options.isProd ? webpack.hash : '' %>.js"></script>

<?php wp_footer(); ?>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.7.2/js/lightgallery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>


</body>

</html>