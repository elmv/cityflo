const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const prod = ("production" === process.env.NODE_ENV);

const bundleCss = prod ? 'style[hash].css' : 'style.css';
const bundlejs = prod ? 'bundle[hash].js' : 'bundle.js';

module.exports = {
    entry: ["./scripts/main.js", "./style/main.sass"],
    output: {
        path: path.resolve('.'),
        filename: 'wordpress/wp-content/themes/cityflo/scripts/' + bundlejs,
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'stage-3'],
                    plugins: ['transform-class-properties']
                }
            },
            {
                test: /\.sass$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {loader: 'css-loader', options: {sourceMap: true}},
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                config: {
                                    path: 'postcss.config.js'
                                }
                            }
                        },
                        {loader: 'sass-loader', options: {sourceMap: true}},
                        {loader: 'import-glob-loader'}
                    ]
                })
            }
        ]
    },
    devtool: prod ? "(none)" : "source-map",
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: prod ? true : false,
            debug: prod ? false : true,
            options: {}
        }),
        new ExtractTextPlugin({
            filename: 'wordpress/wp-content/themes/cityflo/style/' + bundleCss,
            disable: false,
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            isProd: prod,
            template: 'wordpress/wp-content/themes/cityflo/header-tpl.php',
            filename: 'wordpress/wp-content/themes/cityflo/header.php',
            inject: false,
        }),
        new HtmlWebpackPlugin({
            isProd: prod,
            template: 'wordpress/wp-content/themes/cityflo/footer-tpl.php',
            filename: 'wordpress/wp-content/themes/cityflo/footer.php',
            inject: false,
        })
    ],
    devServer: {
        contentBase: path.resolve('.'),
        historyApiFallback: true,
        disableHostCheck: true,
        port: 8010,
    }
};
